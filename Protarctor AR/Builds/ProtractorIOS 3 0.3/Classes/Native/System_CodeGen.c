﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void System.Collections.Generic.Stack`1::.ctor()
// 0x00000002 System.Int32 System.Collections.Generic.Stack`1::get_Count()
// 0x00000003 System.Void System.Collections.Generic.Stack`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000004 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000005 System.Collections.IEnumerator System.Collections.Generic.Stack`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000006 T System.Collections.Generic.Stack`1::Peek()
// 0x00000007 T System.Collections.Generic.Stack`1::Pop()
// 0x00000008 System.Void System.Collections.Generic.Stack`1::Push(T)
// 0x00000009 System.Void System.Collections.Generic.Stack`1::ThrowForEmptyStack()
// 0x0000000A System.Void System.Collections.Generic.Stack`1_Enumerator::.ctor(System.Collections.Generic.Stack`1<T>)
// 0x0000000B System.Void System.Collections.Generic.Stack`1_Enumerator::Dispose()
// 0x0000000C System.Boolean System.Collections.Generic.Stack`1_Enumerator::MoveNext()
// 0x0000000D T System.Collections.Generic.Stack`1_Enumerator::get_Current()
// 0x0000000E System.Void System.Collections.Generic.Stack`1_Enumerator::ThrowEnumerationNotStartedOrEnded()
// 0x0000000F System.Object System.Collections.Generic.Stack`1_Enumerator::System.Collections.IEnumerator.get_Current()
static Il2CppMethodPointer s_methodPointers[15] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[15] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000002, { 0, 6 } },
	{ 0x02000003, { 6, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[9] = 
{
	{ (Il2CppRGCTXDataType)2, 12154 },
	{ (Il2CppRGCTXDataType)2, 12155 },
	{ (Il2CppRGCTXDataType)3, 8680 },
	{ (Il2CppRGCTXDataType)3, 8681 },
	{ (Il2CppRGCTXDataType)3, 8682 },
	{ (Il2CppRGCTXDataType)3, 8683 },
	{ (Il2CppRGCTXDataType)3, 8684 },
	{ (Il2CppRGCTXDataType)3, 8685 },
	{ (Il2CppRGCTXDataType)2, 9787 },
};
extern const Il2CppCodeGenModule g_SystemCodeGenModule;
const Il2CppCodeGenModule g_SystemCodeGenModule = 
{
	"System.dll",
	15,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	9,
	s_rgctxValues,
	NULL,
};
