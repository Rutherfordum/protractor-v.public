﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void NativeToolkit::add_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern void NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4 ();
// 0x00000002 System.Void NativeToolkit::remove_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern void NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1 ();
// 0x00000003 System.Void NativeToolkit::add_OnScreenshotSaved(System.Action`1<System.String>)
extern void NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352 ();
// 0x00000004 System.Void NativeToolkit::remove_OnScreenshotSaved(System.Action`1<System.String>)
extern void NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F ();
// 0x00000005 System.Void NativeToolkit::add_OnImageSaved(System.Action`1<System.String>)
extern void NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3 ();
// 0x00000006 System.Void NativeToolkit::remove_OnImageSaved(System.Action`1<System.String>)
extern void NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35 ();
// 0x00000007 System.Void NativeToolkit::add_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE ();
// 0x00000008 System.Void NativeToolkit::remove_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476 ();
// 0x00000009 System.Void NativeToolkit::add_OnDialogComplete(System.Action`1<System.Boolean>)
extern void NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC ();
// 0x0000000A System.Void NativeToolkit::remove_OnDialogComplete(System.Action`1<System.Boolean>)
extern void NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC ();
// 0x0000000B System.Void NativeToolkit::add_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C ();
// 0x0000000C System.Void NativeToolkit::remove_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE ();
// 0x0000000D System.Int32 NativeToolkit::saveToGallery(System.String)
extern void NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC ();
// 0x0000000E System.Void NativeToolkit::pickImage()
extern void NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754 ();
// 0x0000000F System.Void NativeToolkit::openCamera()
extern void NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7 ();
// 0x00000010 System.Void NativeToolkit::showAlert(System.String,System.String,System.String)
extern void NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B ();
// 0x00000011 NativeToolkit NativeToolkit::get_Instance()
extern void NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6 ();
// 0x00000012 System.Void NativeToolkit::Awake()
extern void NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76 ();
// 0x00000013 System.Void NativeToolkit::SaveScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern void NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB ();
// 0x00000014 System.Collections.IEnumerator NativeToolkit::GrabScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern void NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA ();
// 0x00000015 System.Void NativeToolkit::SaveImage(UnityEngine.Texture2D,System.String,System.String)
extern void NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD ();
// 0x00000016 System.Collections.IEnumerator NativeToolkit::Save(System.Byte[],System.String,System.String,NativeToolkit_ImageType)
extern void NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F ();
// 0x00000017 System.Void NativeToolkit::PickImage()
extern void NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE ();
// 0x00000018 System.Void NativeToolkit::OnPickImage(System.String)
extern void NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30 ();
// 0x00000019 System.Void NativeToolkit::TakeCameraShot()
extern void NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E ();
// 0x0000001A System.Void NativeToolkit::OnCameraFinished(System.String)
extern void NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC ();
// 0x0000001B System.Void NativeToolkit::ShowAlert(System.String,System.String,System.Action`1<System.Boolean>,System.String)
extern void NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2 ();
// 0x0000001C System.Void NativeToolkit::OnDialogPress(System.String)
extern void NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E ();
// 0x0000001D UnityEngine.Texture2D NativeToolkit::LoadImageFromFile(System.String)
extern void NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0 ();
// 0x0000001E System.Collections.IEnumerator NativeToolkit::Wait(System.Single)
extern void NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE ();
// 0x0000001F System.Void NativeToolkit::.ctor()
extern void NativeToolkit__ctor_m385E36B7410BADB983BB42E07067986E46F3CEFC ();
// 0x00000020 System.Void NativeToolkit_<GrabScreenshot>d__30::.ctor(System.Int32)
extern void U3CGrabScreenshotU3Ed__30__ctor_mE9C27BC1166709434CAF40D4004ABAB4E3E84597 ();
// 0x00000021 System.Void NativeToolkit_<GrabScreenshot>d__30::System.IDisposable.Dispose()
extern void U3CGrabScreenshotU3Ed__30_System_IDisposable_Dispose_m8AEC7B08175BA231FB919A5A885B4977D603FBA6 ();
// 0x00000022 System.Boolean NativeToolkit_<GrabScreenshot>d__30::MoveNext()
extern void U3CGrabScreenshotU3Ed__30_MoveNext_m988CDF05BFAC0D86EA8E632E17B54DC33E28FC7C ();
// 0x00000023 System.Object NativeToolkit_<GrabScreenshot>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGrabScreenshotU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621F8D815CC1CC396D7783388B7CB95114C89593 ();
// 0x00000024 System.Object NativeToolkit_<GrabScreenshot>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CGrabScreenshotU3Ed__30_System_Collections_IEnumerator_get_Current_m4353553BC4D04F4FFEAFFB984EE50E30A3D4A917 ();
// 0x00000025 System.Void NativeToolkit_<Save>d__32::.ctor(System.Int32)
extern void U3CSaveU3Ed__32__ctor_m67BD95576F3B8D778601ABF047E4FAD9813640D8 ();
// 0x00000026 System.Void NativeToolkit_<Save>d__32::System.IDisposable.Dispose()
extern void U3CSaveU3Ed__32_System_IDisposable_Dispose_m37C5DFF6EB8DA62D3801BF4821EC0605AB854F68 ();
// 0x00000027 System.Boolean NativeToolkit_<Save>d__32::MoveNext()
extern void U3CSaveU3Ed__32_MoveNext_mE2C978BE5590BFF0D016F75D9DD24AAB8CB566ED ();
// 0x00000028 System.Object NativeToolkit_<Save>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA888257B7430A0F0B852DF6A0E3B9AA9B589A08E ();
// 0x00000029 System.Object NativeToolkit_<Save>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CSaveU3Ed__32_System_Collections_IEnumerator_get_Current_m16ACA322E86938A6BBB7355E2DDC77D4B038B150 ();
// 0x0000002A System.Void NativeToolkit_<Wait>d__40::.ctor(System.Int32)
extern void U3CWaitU3Ed__40__ctor_mF3184DE97B1F4017C0FFD3D7328B554244B45699 ();
// 0x0000002B System.Void NativeToolkit_<Wait>d__40::System.IDisposable.Dispose()
extern void U3CWaitU3Ed__40_System_IDisposable_Dispose_mF774E32F036659DBB80F74BAACE24CE136CF9968 ();
// 0x0000002C System.Boolean NativeToolkit_<Wait>d__40::MoveNext()
extern void U3CWaitU3Ed__40_MoveNext_m92EB8C93147F80177ED3C9D93AA3C69B270D6913 ();
// 0x0000002D System.Object NativeToolkit_<Wait>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11792DBCD4DDDC59B9FAD45527DEB7389DB22FB7 ();
// 0x0000002E System.Object NativeToolkit_<Wait>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CWaitU3Ed__40_System_Collections_IEnumerator_get_Current_m5D28DF754C63B87510EF7B9FCFBDB422F5DD4C91 ();
// 0x0000002F System.Single AngleCalculator::AngleTwoPoint(UnityEngine.Transform,UnityEngine.Transform)
extern void AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9 ();
// 0x00000030 System.Single AngleCalculator::AngleThreePoint(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
extern void AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF ();
// 0x00000031 System.Void AngleCalculator::DrawLine(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.Single)
extern void AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7 ();
// 0x00000032 System.Boolean AngleCalculator::Point(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&)
extern void AngleCalculator_Point_mCF2BB34CB495EDFF20DC958840C5967800C40B01 ();
// 0x00000033 System.Void AngleCalculator::.ctor()
extern void AngleCalculator__ctor_m845B37C4E93EA0E798B32995421F0E8E8D816E54 ();
// 0x00000034 System.Void CameraIOS::Start()
extern void CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770 ();
// 0x00000035 System.Void CameraIOS::OnEnable()
extern void CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D ();
// 0x00000036 System.Void CameraIOS::OnDisable()
extern void CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC ();
// 0x00000037 System.Void CameraIOS::OnSaveScreenshotPress()
extern void CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB ();
// 0x00000038 System.Void CameraIOS::OnCameraPress()
extern void CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07 ();
// 0x00000039 System.Void CameraIOS::OnPickImagePress()
extern void CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01 ();
// 0x0000003A System.Void CameraIOS::ScreenshotSaved(System.String)
extern void CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD ();
// 0x0000003B System.Void CameraIOS::CameraShotComplete(UnityEngine.Texture2D,System.String)
extern void CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF ();
// 0x0000003C System.Void CameraIOS::ImagePicked(UnityEngine.Texture2D,System.String)
extern void CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE ();
// 0x0000003D System.Void CameraIOS::.ctor()
extern void CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE ();
// 0x0000003E System.Void DragMouse::Start()
extern void DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663 ();
// 0x0000003F System.Void DragMouse::OnMouseDrag()
extern void DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7 ();
// 0x00000040 System.Void DragMouse::.ctor()
extern void DragMouse__ctor_m50A6B2A45958A4A88B55841255E8B1CDCBC7C462 ();
// 0x00000041 System.Void GameManager::Update()
extern void GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB ();
// 0x00000042 System.Void GameManager::EnabledMode(System.Int32)
extern void GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231 ();
// 0x00000043 System.Boolean GameManager::Point(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&)
extern void GameManager_Point_m01E549F83667567613DB58677D5BFB8CE32323BF ();
// 0x00000044 System.Void GameManager::.ctor()
extern void GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358 ();
// 0x00000045 System.Void InputDevice::MoveRotateObject(UnityEngine.GameObject,System.Single)
extern void InputDevice_MoveRotateObject_mBD9C742856F296B231729F7C724BEAB4059B668B ();
// 0x00000046 System.Void InputDevice::ScaleObject(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void InputDevice_ScaleObject_mFA7B7FB0C5EB87883EBE6F58A66A884358F47CCF ();
// 0x00000047 System.Void InputDevice::SizeCamera(System.Single,System.Single,System.Single)
extern void InputDevice_SizeCamera_m3A13C55F554BAA772FED1D5F3A63B21450AC6A32 ();
// 0x00000048 System.Void InputDevice::MoveCamera(System.Single,System.Single)
extern void InputDevice_MoveCamera_mAD10067723D57BAE67F31D74439F79C9F32B41CF ();
// 0x00000049 System.Void InputDevice::.ctor()
extern void InputDevice__ctor_m31DFD02145365642DB55AA858E64B376D791E2AC ();
// 0x0000004A System.Void ManagerUI::OpenScene(System.Int32)
extern void ManagerUI_OpenScene_m5BA6194AFD241A7EE0D921E7CEBE0B55886B6604 ();
// 0x0000004B System.Void ManagerUI::AngleMode(System.Int32)
extern void ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E ();
// 0x0000004C System.Void ManagerUI::.ctor()
extern void ManagerUI__ctor_m80A5DD6AE73EA6774FA54E8107C66D9204E5DF50 ();
static Il2CppMethodPointer s_methodPointers[76] = 
{
	NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4,
	NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1,
	NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352,
	NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F,
	NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3,
	NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35,
	NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE,
	NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476,
	NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC,
	NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC,
	NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C,
	NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE,
	NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC,
	NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754,
	NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7,
	NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B,
	NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6,
	NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76,
	NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB,
	NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA,
	NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD,
	NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F,
	NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE,
	NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30,
	NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E,
	NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC,
	NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2,
	NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E,
	NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0,
	NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE,
	NativeToolkit__ctor_m385E36B7410BADB983BB42E07067986E46F3CEFC,
	U3CGrabScreenshotU3Ed__30__ctor_mE9C27BC1166709434CAF40D4004ABAB4E3E84597,
	U3CGrabScreenshotU3Ed__30_System_IDisposable_Dispose_m8AEC7B08175BA231FB919A5A885B4977D603FBA6,
	U3CGrabScreenshotU3Ed__30_MoveNext_m988CDF05BFAC0D86EA8E632E17B54DC33E28FC7C,
	U3CGrabScreenshotU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621F8D815CC1CC396D7783388B7CB95114C89593,
	U3CGrabScreenshotU3Ed__30_System_Collections_IEnumerator_get_Current_m4353553BC4D04F4FFEAFFB984EE50E30A3D4A917,
	U3CSaveU3Ed__32__ctor_m67BD95576F3B8D778601ABF047E4FAD9813640D8,
	U3CSaveU3Ed__32_System_IDisposable_Dispose_m37C5DFF6EB8DA62D3801BF4821EC0605AB854F68,
	U3CSaveU3Ed__32_MoveNext_mE2C978BE5590BFF0D016F75D9DD24AAB8CB566ED,
	U3CSaveU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA888257B7430A0F0B852DF6A0E3B9AA9B589A08E,
	U3CSaveU3Ed__32_System_Collections_IEnumerator_get_Current_m16ACA322E86938A6BBB7355E2DDC77D4B038B150,
	U3CWaitU3Ed__40__ctor_mF3184DE97B1F4017C0FFD3D7328B554244B45699,
	U3CWaitU3Ed__40_System_IDisposable_Dispose_mF774E32F036659DBB80F74BAACE24CE136CF9968,
	U3CWaitU3Ed__40_MoveNext_m92EB8C93147F80177ED3C9D93AA3C69B270D6913,
	U3CWaitU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11792DBCD4DDDC59B9FAD45527DEB7389DB22FB7,
	U3CWaitU3Ed__40_System_Collections_IEnumerator_get_Current_m5D28DF754C63B87510EF7B9FCFBDB422F5DD4C91,
	AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9,
	AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF,
	AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7,
	AngleCalculator_Point_mCF2BB34CB495EDFF20DC958840C5967800C40B01,
	AngleCalculator__ctor_m845B37C4E93EA0E798B32995421F0E8E8D816E54,
	CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770,
	CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D,
	CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC,
	CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB,
	CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07,
	CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01,
	CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD,
	CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF,
	CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE,
	CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE,
	DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663,
	DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7,
	DragMouse__ctor_m50A6B2A45958A4A88B55841255E8B1CDCBC7C462,
	GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB,
	GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231,
	GameManager_Point_m01E549F83667567613DB58677D5BFB8CE32323BF,
	GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358,
	InputDevice_MoveRotateObject_mBD9C742856F296B231729F7C724BEAB4059B668B,
	InputDevice_ScaleObject_mFA7B7FB0C5EB87883EBE6F58A66A884358F47CCF,
	InputDevice_SizeCamera_m3A13C55F554BAA772FED1D5F3A63B21450AC6A32,
	InputDevice_MoveCamera_mAD10067723D57BAE67F31D74439F79C9F32B41CF,
	InputDevice__ctor_m31DFD02145365642DB55AA858E64B376D791E2AC,
	ManagerUI_OpenScene_m5BA6194AFD241A7EE0D921E7CEBE0B55886B6604,
	ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E,
	ManagerUI__ctor_m80A5DD6AE73EA6774FA54E8107C66D9204E5DF50,
};
static const int32_t s_InvokerIndices[76] = 
{
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	91,
	3,
	3,
	516,
	4,
	19,
	1350,
	1351,
	516,
	378,
	3,
	22,
	3,
	22,
	1352,
	22,
	0,
	1265,
	19,
	28,
	19,
	104,
	11,
	11,
	28,
	19,
	104,
	11,
	11,
	28,
	19,
	104,
	11,
	11,
	191,
	1353,
	1354,
	1355,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	22,
	30,
	30,
	19,
	19,
	19,
	19,
	19,
	28,
	1356,
	19,
	1123,
	1357,
	1358,
	1359,
	19,
	28,
	28,
	19,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	76,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
