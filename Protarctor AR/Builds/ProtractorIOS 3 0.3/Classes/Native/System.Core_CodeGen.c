﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000013 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000014 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000016 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000018 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000019 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001C System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000001F System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000020 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000021 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000022 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000023 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000024 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000025 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000027 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000028 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000029 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000002A System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000002B System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000002C System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002D System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000002E System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000002F System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000030 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000031 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000032 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000033 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000034 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000035 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000036 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000037 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000038 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000039 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000003A T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000003B System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
static Il2CppMethodPointer s_methodPointers[59] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[59] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x02000004, { 22, 3 } },
	{ 0x02000005, { 25, 9 } },
	{ 0x02000006, { 34, 7 } },
	{ 0x02000007, { 41, 9 } },
	{ 0x02000008, { 50, 1 } },
	{ 0x02000009, { 51, 21 } },
	{ 0x0200000B, { 72, 1 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[73] = 
{
	{ (Il2CppRGCTXDataType)2, 12156 },
	{ (Il2CppRGCTXDataType)3, 8686 },
	{ (Il2CppRGCTXDataType)2, 12157 },
	{ (Il2CppRGCTXDataType)2, 12158 },
	{ (Il2CppRGCTXDataType)3, 8687 },
	{ (Il2CppRGCTXDataType)2, 12159 },
	{ (Il2CppRGCTXDataType)2, 12160 },
	{ (Il2CppRGCTXDataType)3, 8688 },
	{ (Il2CppRGCTXDataType)2, 12161 },
	{ (Il2CppRGCTXDataType)3, 8689 },
	{ (Il2CppRGCTXDataType)2, 12162 },
	{ (Il2CppRGCTXDataType)3, 8690 },
	{ (Il2CppRGCTXDataType)3, 8691 },
	{ (Il2CppRGCTXDataType)2, 9803 },
	{ (Il2CppRGCTXDataType)3, 8692 },
	{ (Il2CppRGCTXDataType)2, 9805 },
	{ (Il2CppRGCTXDataType)2, 12163 },
	{ (Il2CppRGCTXDataType)3, 8693 },
	{ (Il2CppRGCTXDataType)2, 9808 },
	{ (Il2CppRGCTXDataType)2, 9810 },
	{ (Il2CppRGCTXDataType)2, 12164 },
	{ (Il2CppRGCTXDataType)3, 8694 },
	{ (Il2CppRGCTXDataType)3, 8695 },
	{ (Il2CppRGCTXDataType)2, 9815 },
	{ (Il2CppRGCTXDataType)3, 8696 },
	{ (Il2CppRGCTXDataType)3, 8697 },
	{ (Il2CppRGCTXDataType)2, 9824 },
	{ (Il2CppRGCTXDataType)2, 12165 },
	{ (Il2CppRGCTXDataType)3, 8698 },
	{ (Il2CppRGCTXDataType)3, 8699 },
	{ (Il2CppRGCTXDataType)2, 9826 },
	{ (Il2CppRGCTXDataType)2, 12058 },
	{ (Il2CppRGCTXDataType)3, 8700 },
	{ (Il2CppRGCTXDataType)3, 8701 },
	{ (Il2CppRGCTXDataType)3, 8702 },
	{ (Il2CppRGCTXDataType)2, 9833 },
	{ (Il2CppRGCTXDataType)2, 12166 },
	{ (Il2CppRGCTXDataType)3, 8703 },
	{ (Il2CppRGCTXDataType)3, 8704 },
	{ (Il2CppRGCTXDataType)3, 8390 },
	{ (Il2CppRGCTXDataType)3, 8705 },
	{ (Il2CppRGCTXDataType)3, 8706 },
	{ (Il2CppRGCTXDataType)2, 9842 },
	{ (Il2CppRGCTXDataType)2, 12167 },
	{ (Il2CppRGCTXDataType)3, 8707 },
	{ (Il2CppRGCTXDataType)3, 8708 },
	{ (Il2CppRGCTXDataType)3, 8709 },
	{ (Il2CppRGCTXDataType)3, 8710 },
	{ (Il2CppRGCTXDataType)3, 8395 },
	{ (Il2CppRGCTXDataType)3, 8711 },
	{ (Il2CppRGCTXDataType)3, 8712 },
	{ (Il2CppRGCTXDataType)3, 8713 },
	{ (Il2CppRGCTXDataType)2, 12168 },
	{ (Il2CppRGCTXDataType)3, 8714 },
	{ (Il2CppRGCTXDataType)3, 8715 },
	{ (Il2CppRGCTXDataType)3, 8716 },
	{ (Il2CppRGCTXDataType)2, 9856 },
	{ (Il2CppRGCTXDataType)3, 8717 },
	{ (Il2CppRGCTXDataType)3, 8718 },
	{ (Il2CppRGCTXDataType)2, 9859 },
	{ (Il2CppRGCTXDataType)3, 8719 },
	{ (Il2CppRGCTXDataType)1, 12169 },
	{ (Il2CppRGCTXDataType)2, 9858 },
	{ (Il2CppRGCTXDataType)3, 8720 },
	{ (Il2CppRGCTXDataType)1, 9858 },
	{ (Il2CppRGCTXDataType)1, 9856 },
	{ (Il2CppRGCTXDataType)2, 12170 },
	{ (Il2CppRGCTXDataType)2, 9858 },
	{ (Il2CppRGCTXDataType)3, 8721 },
	{ (Il2CppRGCTXDataType)3, 8722 },
	{ (Il2CppRGCTXDataType)3, 8723 },
	{ (Il2CppRGCTXDataType)2, 9857 },
	{ (Il2CppRGCTXDataType)2, 9870 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	59,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	73,
	s_rgctxValues,
	NULL,
};
