﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77;
// System.Action`2<UnityEngine.Texture2D,System.String>
struct Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};


// System.Object


// NativeToolkit_<Wait>d__40
struct  U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Wait>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Wait>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single NativeToolkit_<Wait>d__40::delay
	float ___delay_2;
	// System.Single NativeToolkit_<Wait>d__40::<pauseTarget>5__2
	float ___U3CpauseTargetU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CpauseTargetU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CpauseTargetU3E5__2_3)); }
	inline float get_U3CpauseTargetU3E5__2_3() const { return ___U3CpauseTargetU3E5__2_3; }
	inline float* get_address_of_U3CpauseTargetU3E5__2_3() { return &___U3CpauseTargetU3E5__2_3; }
	inline void set_U3CpauseTargetU3E5__2_3(float value)
	{
		___U3CpauseTargetU3E5__2_3 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8__padding[12];
	};

public:
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// NativeToolkit_<GrabScreenshot>d__30
struct  U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<GrabScreenshot>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<GrabScreenshot>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Rect NativeToolkit_<GrabScreenshot>d__30::screenArea
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea_2;
	// System.String NativeToolkit_<GrabScreenshot>d__30::fileType
	String_t* ___fileType_3;
	// System.String NativeToolkit_<GrabScreenshot>d__30::fileName
	String_t* ___fileName_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_screenArea_2() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___screenArea_2)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_screenArea_2() const { return ___screenArea_2; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_screenArea_2() { return &___screenArea_2; }
	inline void set_screenArea_2(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___screenArea_2 = value;
	}

	inline static int32_t get_offset_of_fileType_3() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___fileType_3)); }
	inline String_t* get_fileType_3() const { return ___fileType_3; }
	inline String_t** get_address_of_fileType_3() { return &___fileType_3; }
	inline void set_fileType_3(String_t* value)
	{
		___fileType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileType_3), (void*)value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileName_4), (void*)value);
	}
};


// NativeToolkit_ImageType
struct  ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130 
{
public:
	// System.Int32 NativeToolkit_ImageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NativeToolkit_SaveStatus
struct  SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC 
{
public:
	// System.Int32 NativeToolkit_SaveStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// NativeToolkit_<Save>d__32
struct  U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Save>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Save>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String NativeToolkit_<Save>d__32::path
	String_t* ___path_2;
	// System.Byte[] NativeToolkit_<Save>d__32::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// NativeToolkit_ImageType NativeToolkit_<Save>d__32::imageType
	int32_t ___imageType_4;
	// System.Int32 NativeToolkit_<Save>d__32::<count>5__2
	int32_t ___U3CcountU3E5__2_5;
	// NativeToolkit_SaveStatus NativeToolkit_<Save>d__32::<saved>5__3
	int32_t ___U3CsavedU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_2), (void*)value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_imageType_4() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___imageType_4)); }
	inline int32_t get_imageType_4() const { return ___imageType_4; }
	inline int32_t* get_address_of_imageType_4() { return &___imageType_4; }
	inline void set_imageType_4(int32_t value)
	{
		___imageType_4 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CcountU3E5__2_5)); }
	inline int32_t get_U3CcountU3E5__2_5() const { return ___U3CcountU3E5__2_5; }
	inline int32_t* get_address_of_U3CcountU3E5__2_5() { return &___U3CcountU3E5__2_5; }
	inline void set_U3CcountU3E5__2_5(int32_t value)
	{
		___U3CcountU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CsavedU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CsavedU3E5__3_6)); }
	inline int32_t get_U3CsavedU3E5__3_6() const { return ___U3CsavedU3E5__3_6; }
	inline int32_t* get_address_of_U3CsavedU3E5__3_6() { return &___U3CsavedU3E5__3_6; }
	inline void set_U3CsavedU3E5__3_6(int32_t value)
	{
		___U3CsavedU3E5__3_6 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// AngleCalculator
struct  AngleCalculator_tDACEF6B6E9AA9576D1B11E23FC4306DEA31388C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// CameraIOS
struct  CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image CameraIOS::Photo
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Photo_4;
	// System.String CameraIOS::Activate
	String_t* ___Activate_5;

public:
	inline static int32_t get_offset_of_Photo_4() { return static_cast<int32_t>(offsetof(CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596, ___Photo_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Photo_4() const { return ___Photo_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Photo_4() { return &___Photo_4; }
	inline void set_Photo_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Photo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Photo_4), (void*)value);
	}

	inline static int32_t get_offset_of_Activate_5() { return static_cast<int32_t>(offsetof(CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596, ___Activate_5)); }
	inline String_t* get_Activate_5() const { return ___Activate_5; }
	inline String_t** get_address_of_Activate_5() { return &___Activate_5; }
	inline void set_Activate_5(String_t* value)
	{
		___Activate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Activate_5), (void*)value);
	}
};


// DragMouse
struct  DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera DragMouse::MainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___MainCamera_4;
	// UnityEngine.Vector3 DragMouse::poseInput
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___poseInput_5;
	// UnityEngine.Vector3 DragMouse::poseTarget
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___poseTarget_6;
	// System.Single DragMouse::z
	float ___z_7;
	// System.Int32 DragMouse::id
	int32_t ___id_9;

public:
	inline static int32_t get_offset_of_MainCamera_4() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___MainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_MainCamera_4() const { return ___MainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_MainCamera_4() { return &___MainCamera_4; }
	inline void set_MainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___MainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_poseInput_5() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___poseInput_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_poseInput_5() const { return ___poseInput_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_poseInput_5() { return &___poseInput_5; }
	inline void set_poseInput_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___poseInput_5 = value;
	}

	inline static int32_t get_offset_of_poseTarget_6() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___poseTarget_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_poseTarget_6() const { return ___poseTarget_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_poseTarget_6() { return &___poseTarget_6; }
	inline void set_poseTarget_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___poseTarget_6 = value;
	}

	inline static int32_t get_offset_of_z_7() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___z_7)); }
	inline float get_z_7() const { return ___z_7; }
	inline float* get_address_of_z_7() { return &___z_7; }
	inline void set_z_7(float value)
	{
		___z_7 = value;
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___id_9)); }
	inline int32_t get_id_9() const { return ___id_9; }
	inline int32_t* get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(int32_t value)
	{
		___id_9 = value;
	}
};

struct DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields
{
public:
	// System.Int32 DragMouse::idPoint
	int32_t ___idPoint_8;

public:
	inline static int32_t get_offset_of_idPoint_8() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields, ___idPoint_8)); }
	inline int32_t get_idPoint_8() const { return ___idPoint_8; }
	inline int32_t* get_address_of_idPoint_8() { return &___idPoint_8; }
	inline void set_idPoint_8(int32_t value)
	{
		___idPoint_8 = value;
	}
};


// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text GameManager::angleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___angleText_4;
	// UnityEngine.Transform GameManager::main
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main_5;
	// UnityEngine.Transform GameManager::one
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one_6;
	// UnityEngine.Transform GameManager::two
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two_7;
	// UnityEngine.Transform GameManager::lineOne
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lineOne_8;
	// UnityEngine.Transform GameManager::lineTwo
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lineTwo_9;
	// UnityEngine.Transform GameManager::lineThree
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lineThree_10;
	// UnityEngine.Transform GameManager::three
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___three_11;
	// UnityEngine.Transform GameManager::four
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___four_12;

public:
	inline static int32_t get_offset_of_angleText_4() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___angleText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_angleText_4() const { return ___angleText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_angleText_4() { return &___angleText_4; }
	inline void set_angleText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___angleText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___angleText_4), (void*)value);
	}

	inline static int32_t get_offset_of_main_5() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___main_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_main_5() const { return ___main_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_main_5() { return &___main_5; }
	inline void set_main_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___main_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_5), (void*)value);
	}

	inline static int32_t get_offset_of_one_6() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___one_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_one_6() const { return ___one_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_one_6() { return &___one_6; }
	inline void set_one_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___one_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___one_6), (void*)value);
	}

	inline static int32_t get_offset_of_two_7() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___two_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_two_7() const { return ___two_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_two_7() { return &___two_7; }
	inline void set_two_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___two_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___two_7), (void*)value);
	}

	inline static int32_t get_offset_of_lineOne_8() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___lineOne_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lineOne_8() const { return ___lineOne_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lineOne_8() { return &___lineOne_8; }
	inline void set_lineOne_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lineOne_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineOne_8), (void*)value);
	}

	inline static int32_t get_offset_of_lineTwo_9() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___lineTwo_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lineTwo_9() const { return ___lineTwo_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lineTwo_9() { return &___lineTwo_9; }
	inline void set_lineTwo_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lineTwo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineTwo_9), (void*)value);
	}

	inline static int32_t get_offset_of_lineThree_10() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___lineThree_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lineThree_10() const { return ___lineThree_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lineThree_10() { return &___lineThree_10; }
	inline void set_lineThree_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lineThree_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineThree_10), (void*)value);
	}

	inline static int32_t get_offset_of_three_11() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___three_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_three_11() const { return ___three_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_three_11() { return &___three_11; }
	inline void set_three_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___three_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___three_11), (void*)value);
	}

	inline static int32_t get_offset_of_four_12() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___four_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_four_12() const { return ___four_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_four_12() { return &___four_12; }
	inline void set_four_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___four_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___four_12), (void*)value);
	}
};


// ManagerUI
struct  ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields
{
public:
	// System.Int32 ManagerUI::ModeAngle
	int32_t ___ModeAngle_4;

public:
	inline static int32_t get_offset_of_ModeAngle_4() { return static_cast<int32_t>(offsetof(ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields, ___ModeAngle_4)); }
	inline int32_t get_ModeAngle_4() const { return ___ModeAngle_4; }
	inline int32_t* get_address_of_ModeAngle_4() { return &___ModeAngle_4; }
	inline void set_ModeAngle_4(int32_t value)
	{
		___ModeAngle_4 = value;
	}
};


// NativeToolkit
struct  NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields
{
public:
	// System.Action`1<UnityEngine.Texture2D> NativeToolkit::OnScreenshotTaken
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * ___OnScreenshotTaken_4;
	// System.Action`1<System.String> NativeToolkit::OnScreenshotSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnScreenshotSaved_5;
	// System.Action`1<System.String> NativeToolkit::OnImageSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnImageSaved_6;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnImagePicked
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___OnImagePicked_7;
	// System.Action`1<System.Boolean> NativeToolkit::OnDialogComplete
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___OnDialogComplete_8;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnCameraShotComplete
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___OnCameraShotComplete_9;
	// NativeToolkit NativeToolkit::instance
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * ___instance_10;
	// UnityEngine.GameObject NativeToolkit::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_11;

public:
	inline static int32_t get_offset_of_OnScreenshotTaken_4() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotTaken_4)); }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * get_OnScreenshotTaken_4() const { return ___OnScreenshotTaken_4; }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 ** get_address_of_OnScreenshotTaken_4() { return &___OnScreenshotTaken_4; }
	inline void set_OnScreenshotTaken_4(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * value)
	{
		___OnScreenshotTaken_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnScreenshotTaken_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnScreenshotSaved_5() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotSaved_5)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnScreenshotSaved_5() const { return ___OnScreenshotSaved_5; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnScreenshotSaved_5() { return &___OnScreenshotSaved_5; }
	inline void set_OnScreenshotSaved_5(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnScreenshotSaved_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnScreenshotSaved_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnImageSaved_6() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImageSaved_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnImageSaved_6() const { return ___OnImageSaved_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnImageSaved_6() { return &___OnImageSaved_6; }
	inline void set_OnImageSaved_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnImageSaved_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnImageSaved_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnImagePicked_7() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImagePicked_7)); }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * get_OnImagePicked_7() const { return ___OnImagePicked_7; }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 ** get_address_of_OnImagePicked_7() { return &___OnImagePicked_7; }
	inline void set_OnImagePicked_7(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * value)
	{
		___OnImagePicked_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnImagePicked_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnDialogComplete_8() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnDialogComplete_8)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_OnDialogComplete_8() const { return ___OnDialogComplete_8; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_OnDialogComplete_8() { return &___OnDialogComplete_8; }
	inline void set_OnDialogComplete_8(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___OnDialogComplete_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDialogComplete_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnCameraShotComplete_9() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnCameraShotComplete_9)); }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * get_OnCameraShotComplete_9() const { return ___OnCameraShotComplete_9; }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 ** get_address_of_OnCameraShotComplete_9() { return &___OnCameraShotComplete_9; }
	inline void set_OnCameraShotComplete_9(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * value)
	{
		___OnCameraShotComplete_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnCameraShotComplete_9), (void*)value);
	}

	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___instance_10)); }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * get_instance_10() const { return ___instance_10; }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_10), (void*)value);
	}

	inline static int32_t get_offset_of_go_11() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___go_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_11() const { return ___go_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_11() { return &___go_11; }
	inline void set_go_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___go_11), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t7F98A3A922EF4B6DA62C3CF2D4E5897EED2C26B8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F), -1, sizeof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2031[8] = 
{
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnScreenshotTaken_4(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnScreenshotSaved_5(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnImageSaved_6(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnImagePicked_7(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnDialogComplete_8(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnCameraShotComplete_9(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_instance_10(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_go_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2032[3] = 
{
	ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2033[5] = 
{
	SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2034[5] = 
{
	U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5::get_offset_of_U3CU3E1__state_0(),
	U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5::get_offset_of_U3CU3E2__current_1(),
	U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5::get_offset_of_screenArea_2(),
	U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5::get_offset_of_fileType_3(),
	U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5::get_offset_of_fileName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2035[7] = 
{
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_U3CU3E1__state_0(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_U3CU3E2__current_1(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_path_2(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_bytes_3(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_imageType_4(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_U3CcountU3E5__2_5(),
	U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD::get_offset_of_U3CsavedU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2036[4] = 
{
	U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8::get_offset_of_U3CU3E1__state_0(),
	U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8::get_offset_of_U3CU3E2__current_1(),
	U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8::get_offset_of_delay_2(),
	U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8::get_offset_of_U3CpauseTargetU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (AngleCalculator_tDACEF6B6E9AA9576D1B11E23FC4306DEA31388C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2038[2] = 
{
	CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596::get_offset_of_Photo_4(),
	CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596::get_offset_of_Activate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A), -1, sizeof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2039[6] = 
{
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A::get_offset_of_MainCamera_4(),
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A::get_offset_of_poseInput_5(),
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A::get_offset_of_poseTarget_6(),
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A::get_offset_of_z_7(),
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields::get_offset_of_idPoint_8(),
	DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A::get_offset_of_id_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2040[9] = 
{
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_angleText_4(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_main_5(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_one_6(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_two_7(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_lineOne_8(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_lineTwo_9(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_lineThree_10(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_three_11(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_four_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE), -1, sizeof(ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2041[1] = 
{
	ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields::get_offset_of_ModeAngle_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
