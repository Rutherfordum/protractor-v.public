﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void NativeToolkitExample::Start()
extern void NativeToolkitExample_Start_m417A512966991FE9BD602AB1AD3FDA51A8872CFF ();
// 0x00000002 System.Void NativeToolkitExample::OnEnable()
extern void NativeToolkitExample_OnEnable_mBF6629566DF71598D43DF70119FA5751DA0A6CFC ();
// 0x00000003 System.Void NativeToolkitExample::OnDisable()
extern void NativeToolkitExample_OnDisable_m244A01917182A9D39CDECE9E1DD8315EFE3CCB37 ();
// 0x00000004 System.Void NativeToolkitExample::OnSaveScreenshotPress()
extern void NativeToolkitExample_OnSaveScreenshotPress_m5C7808E82FC15D08FF68F350E7192E8627047927 ();
// 0x00000005 System.Void NativeToolkitExample::OnSaveImagePress()
extern void NativeToolkitExample_OnSaveImagePress_mB037A647D51D3CA33E54DFF28DA00EEFEAC410AE ();
// 0x00000006 System.Void NativeToolkitExample::OnPickImagePress()
extern void NativeToolkitExample_OnPickImagePress_m1685B410E7BD84EC4C7B1C766E21C9204C00677B ();
// 0x00000007 System.Void NativeToolkitExample::OnEmailSharePress()
extern void NativeToolkitExample_OnEmailSharePress_m9E9CFF161CDBF0A8135FF12DC542151EF033950F ();
// 0x00000008 System.Void NativeToolkitExample::OnCameraPress()
extern void NativeToolkitExample_OnCameraPress_m0FB310F933339F97A983A3056F1F1008C531A467 ();
// 0x00000009 System.Void NativeToolkitExample::OnPickContactPress()
extern void NativeToolkitExample_OnPickContactPress_mDB1965E2B7AE0A06E04643825660B70D313E718E ();
// 0x0000000A System.Void NativeToolkitExample::OnShowAlertPress()
extern void NativeToolkitExample_OnShowAlertPress_m419BDEC5A279AFE357B24DD234A2AB2AE1B7B692 ();
// 0x0000000B System.Void NativeToolkitExample::OnShowDialogPress()
extern void NativeToolkitExample_OnShowDialogPress_m1B0473A0B9D6018AC0332A56B3A7BF250F19D0B0 ();
// 0x0000000C System.Void NativeToolkitExample::OnLocalNotificationPress()
extern void NativeToolkitExample_OnLocalNotificationPress_mB01E28C07BA5134FA3B8573AD68F71AF041B44D2 ();
// 0x0000000D System.Void NativeToolkitExample::OnClearNotificationsPress()
extern void NativeToolkitExample_OnClearNotificationsPress_mECB8C2D882A7677373D3E52064E78311F6EDB89A ();
// 0x0000000E System.Void NativeToolkitExample::OnGetLocationPress()
extern void NativeToolkitExample_OnGetLocationPress_mC0A35B1CC168462899A80F622C5DC66FD70355EF ();
// 0x0000000F System.Void NativeToolkitExample::OnRateAppPress()
extern void NativeToolkitExample_OnRateAppPress_m65445D8B6923D5D96B2D14D929F2230F28394708 ();
// 0x00000010 System.Void NativeToolkitExample::ScreenshotSaved(System.String)
extern void NativeToolkitExample_ScreenshotSaved_m32D93E731DED22430E69E2EC47E36436DF2F54F0 ();
// 0x00000011 System.Void NativeToolkitExample::ImageSaved(System.String)
extern void NativeToolkitExample_ImageSaved_m1F06EB47F0365F44B8063F6E63D0B676D1DD29FF ();
// 0x00000012 System.Void NativeToolkitExample::ImagePicked(UnityEngine.Texture2D,System.String)
extern void NativeToolkitExample_ImagePicked_mD6B09B18C51462B56D98A627A4CDA61C422FA7BA ();
// 0x00000013 System.Void NativeToolkitExample::CameraShotComplete(UnityEngine.Texture2D,System.String)
extern void NativeToolkitExample_CameraShotComplete_m1079DCA170B7E6BD6437201AFD6B057BAF9BA213 ();
// 0x00000014 System.Void NativeToolkitExample::DialogFinished(System.Boolean)
extern void NativeToolkitExample_DialogFinished_m7912810CE75BBD6B22207192FCA6D4CB85F640DC ();
// 0x00000015 System.Void NativeToolkitExample::AppRated(System.String)
extern void NativeToolkitExample_AppRated_m560C620EF469CDE5B3177A420663B1BF94C55681 ();
// 0x00000016 System.Void NativeToolkitExample::ContactPicked(System.String,System.String,System.String)
extern void NativeToolkitExample_ContactPicked_m7DC34FF9EDEBDDAB618AE99488EA4E5806AAA6D5 ();
// 0x00000017 System.Void NativeToolkitExample::.ctor()
extern void NativeToolkitExample__ctor_m4804840DF1188B930DCE48441CA7F20F04C29FF9 ();
// 0x00000018 System.Void NativeToolkit::add_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern void NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4 ();
// 0x00000019 System.Void NativeToolkit::remove_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern void NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1 ();
// 0x0000001A System.Void NativeToolkit::add_OnScreenshotSaved(System.Action`1<System.String>)
extern void NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352 ();
// 0x0000001B System.Void NativeToolkit::remove_OnScreenshotSaved(System.Action`1<System.String>)
extern void NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F ();
// 0x0000001C System.Void NativeToolkit::add_OnImageSaved(System.Action`1<System.String>)
extern void NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3 ();
// 0x0000001D System.Void NativeToolkit::remove_OnImageSaved(System.Action`1<System.String>)
extern void NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35 ();
// 0x0000001E System.Void NativeToolkit::add_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE ();
// 0x0000001F System.Void NativeToolkit::remove_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476 ();
// 0x00000020 System.Void NativeToolkit::add_OnDialogComplete(System.Action`1<System.Boolean>)
extern void NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC ();
// 0x00000021 System.Void NativeToolkit::remove_OnDialogComplete(System.Action`1<System.Boolean>)
extern void NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC ();
// 0x00000022 System.Void NativeToolkit::add_OnRateComplete(System.Action`1<System.String>)
extern void NativeToolkit_add_OnRateComplete_m89513DC2DA5391CBEF6AE5D11A6B98C571BB8AEF ();
// 0x00000023 System.Void NativeToolkit::remove_OnRateComplete(System.Action`1<System.String>)
extern void NativeToolkit_remove_OnRateComplete_mDC0ED5D7A6E141B8E85D998D1B793F942C58E7CD ();
// 0x00000024 System.Void NativeToolkit::add_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C ();
// 0x00000025 System.Void NativeToolkit::remove_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE ();
// 0x00000026 System.Void NativeToolkit::add_OnContactPicked(System.Action`3<System.String,System.String,System.String>)
extern void NativeToolkit_add_OnContactPicked_mA74824E31F48D3F5724233AC91597C6C3D05F60F ();
// 0x00000027 System.Void NativeToolkit::remove_OnContactPicked(System.Action`3<System.String,System.String,System.String>)
extern void NativeToolkit_remove_OnContactPicked_mC2B069F3D41DCBCD15D27F617237DDCCA21DBB96 ();
// 0x00000028 System.Int32 NativeToolkit::saveToGallery(System.String)
extern void NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC ();
// 0x00000029 System.Void NativeToolkit::pickImage()
extern void NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754 ();
// 0x0000002A System.Void NativeToolkit::openCamera()
extern void NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7 ();
// 0x0000002B System.Void NativeToolkit::pickContact()
extern void NativeToolkit_pickContact_m5B52D7B975AF656010B1DF95DEEA8EE14CA5DCF1 ();
// 0x0000002C System.String NativeToolkit::getLocale()
extern void NativeToolkit_getLocale_m0426AF05D41E201B6B0CAD94BC52DE038FA4ED8F ();
// 0x0000002D System.Void NativeToolkit::sendEmail(System.String,System.String,System.String,System.String,System.String,System.String)
extern void NativeToolkit_sendEmail_m896C9771723C12700898D7965518D70CDC926906 ();
// 0x0000002E System.Void NativeToolkit::scheduleLocalNotification(System.String,System.String,System.String,System.Int32,System.String)
extern void NativeToolkit_scheduleLocalNotification_m7D10ED770DEC3DC3AE18F047A901E93A8A923051 ();
// 0x0000002F System.Void NativeToolkit::clearLocalNotification(System.String)
extern void NativeToolkit_clearLocalNotification_m8FE0A206C3C3C82BDCAF608B86E396E5B4D6D617 ();
// 0x00000030 System.Void NativeToolkit::clearAllLocalNotifications()
extern void NativeToolkit_clearAllLocalNotifications_m65CC6A7D6F8456AE5754D0F0BF1166AC073E76A7 ();
// 0x00000031 System.Boolean NativeToolkit::wasLaunchedFromNotification()
extern void NativeToolkit_wasLaunchedFromNotification_m69282D5647DB107B0C6B9E501383C3747FC8C93F ();
// 0x00000032 System.Void NativeToolkit::rateApp(System.String,System.String,System.String,System.String,System.String,System.String)
extern void NativeToolkit_rateApp_m59D35367851F9C075F418E88C7734172AE335BE3 ();
// 0x00000033 System.Void NativeToolkit::showConfirm(System.String,System.String,System.String,System.String)
extern void NativeToolkit_showConfirm_m8D3BC147FF343E24EA877EB6E87B58BD087F5EFE ();
// 0x00000034 System.Void NativeToolkit::showAlert(System.String,System.String,System.String)
extern void NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B ();
// 0x00000035 System.Void NativeToolkit::startLocation()
extern void NativeToolkit_startLocation_mEE1F8F4801DBFB5C11678245C442A94EF72585B5 ();
// 0x00000036 System.Double NativeToolkit::getLongitude()
extern void NativeToolkit_getLongitude_m7BF826B2F4917F85111E0EEE1282426E8BE12267 ();
// 0x00000037 System.Double NativeToolkit::getLatitude()
extern void NativeToolkit_getLatitude_m7FA134FC8257BB863AE140C5F6486ADC1A75D5BE ();
// 0x00000038 NativeToolkit NativeToolkit::get_Instance()
extern void NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6 ();
// 0x00000039 System.Void NativeToolkit::Awake()
extern void NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76 ();
// 0x0000003A System.Void NativeToolkit::SaveScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern void NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB ();
// 0x0000003B System.Collections.IEnumerator NativeToolkit::GrabScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern void NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA ();
// 0x0000003C System.Void NativeToolkit::SaveImage(UnityEngine.Texture2D,System.String,System.String)
extern void NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD ();
// 0x0000003D System.Collections.IEnumerator NativeToolkit::Save(System.Byte[],System.String,System.String,NativeToolkit_ImageType)
extern void NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F ();
// 0x0000003E System.Void NativeToolkit::PickImage()
extern void NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE ();
// 0x0000003F System.Void NativeToolkit::OnPickImage(System.String)
extern void NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30 ();
// 0x00000040 System.Void NativeToolkit::TakeCameraShot()
extern void NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E ();
// 0x00000041 System.Void NativeToolkit::OnCameraFinished(System.String)
extern void NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC ();
// 0x00000042 System.Void NativeToolkit::PickContact()
extern void NativeToolkit_PickContact_m3B22F28B543CC7CD91342C0CE3F87899F910981C ();
// 0x00000043 System.Void NativeToolkit::OnPickContactFinished(System.String)
extern void NativeToolkit_OnPickContactFinished_m464156AA1AC55C69C053B107985F5BC8ECC33355 ();
// 0x00000044 System.Void NativeToolkit::SendEmail(System.String,System.String,System.String,System.String,System.String,System.String)
extern void NativeToolkit_SendEmail_m25AA52AD39886B040D106E2EE9BE9CB09F71BC08 ();
// 0x00000045 System.Void NativeToolkit::ShowConfirm(System.String,System.String,System.Action`1<System.Boolean>,System.String,System.String)
extern void NativeToolkit_ShowConfirm_mBDCCC1703CB6F049BA940FDB4EDB46808525A8E4 ();
// 0x00000046 System.Void NativeToolkit::ShowAlert(System.String,System.String,System.Action`1<System.Boolean>,System.String)
extern void NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2 ();
// 0x00000047 System.Void NativeToolkit::OnDialogPress(System.String)
extern void NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E ();
// 0x00000048 System.Void NativeToolkit::RateApp(System.String,System.String,System.String,System.String,System.String,System.String,System.Action`1<System.String>)
extern void NativeToolkit_RateApp_mC01ADAD62E4F721F7B69DC5C0D734A717D242E7D ();
// 0x00000049 System.Void NativeToolkit::OnRatePress(System.String)
extern void NativeToolkit_OnRatePress_m57BA5B69E0696927C09EAA16F9B07C2653561346 ();
// 0x0000004A System.Boolean NativeToolkit::StartLocation()
extern void NativeToolkit_StartLocation_m8FCA45DADC62B70F074D78FC8AED57281408BA94 ();
// 0x0000004B System.Double NativeToolkit::GetLongitude()
extern void NativeToolkit_GetLongitude_mA8F0E6D341CA0F3B2A2948F48120C68C0E43870A ();
// 0x0000004C System.Double NativeToolkit::GetLatitude()
extern void NativeToolkit_GetLatitude_m704B406D3C7F0E972880131CE1F4FD5DD4802EB6 ();
// 0x0000004D System.String NativeToolkit::GetCountryCode()
extern void NativeToolkit_GetCountryCode_mB2D3A5D2EC2B8AC9EA762BE0861C8825CB4E7D59 ();
// 0x0000004E System.Void NativeToolkit::ScheduleLocalNotification(System.String,System.String,System.Int32,System.Int32,System.String,System.Boolean,System.String,System.String)
extern void NativeToolkit_ScheduleLocalNotification_m915F92A1B2398362C99A9D1E1259F329A5B3242C ();
// 0x0000004F System.Void NativeToolkit::ClearLocalNotification(System.Int32)
extern void NativeToolkit_ClearLocalNotification_mC69F2CD75F6ED7F7DCFBB5CB38BA47BB8B994A2C ();
// 0x00000050 System.Void NativeToolkit::ClearAllLocalNotifications()
extern void NativeToolkit_ClearAllLocalNotifications_m6D9FDEF752C97B9F4E5E36D57EDBF9B2A221A628 ();
// 0x00000051 System.Boolean NativeToolkit::WasLaunchedFromNotification()
extern void NativeToolkit_WasLaunchedFromNotification_mD48C7B6A2C9A0B77F7FA33413FE29B144F8ABC37 ();
// 0x00000052 UnityEngine.Texture2D NativeToolkit::LoadImageFromFile(System.String)
extern void NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0 ();
// 0x00000053 System.Collections.IEnumerator NativeToolkit::Wait(System.Single)
extern void NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE ();
// 0x00000054 System.Void NativeToolkit::.ctor()
extern void NativeToolkit__ctor_m385E36B7410BADB983BB42E07067986E46F3CEFC ();
// 0x00000055 System.Void NativeToolkit_<GrabScreenshot>d__48::.ctor(System.Int32)
extern void U3CGrabScreenshotU3Ed__48__ctor_m996D01A5467FC6C4255E693A511C813583CE00DE ();
// 0x00000056 System.Void NativeToolkit_<GrabScreenshot>d__48::System.IDisposable.Dispose()
extern void U3CGrabScreenshotU3Ed__48_System_IDisposable_Dispose_m5AE7CDEADC6959C94862E54FFACA3AF795BB8DFF ();
// 0x00000057 System.Boolean NativeToolkit_<GrabScreenshot>d__48::MoveNext()
extern void U3CGrabScreenshotU3Ed__48_MoveNext_m4C317CD3C2E805CDBF675DE7B2F9813C8EF4E775 ();
// 0x00000058 System.Object NativeToolkit_<GrabScreenshot>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGrabScreenshotU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB50117431D009B01F608DF2D02058F83119BDDC7 ();
// 0x00000059 System.Object NativeToolkit_<GrabScreenshot>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CGrabScreenshotU3Ed__48_System_Collections_IEnumerator_get_Current_m3082F429511212FCF2C16A510CA84A955EDD988B ();
// 0x0000005A System.Void NativeToolkit_<Save>d__50::.ctor(System.Int32)
extern void U3CSaveU3Ed__50__ctor_m0CAD84EBC0DF895828EF24C9FE2B45EBFB3E9530 ();
// 0x0000005B System.Void NativeToolkit_<Save>d__50::System.IDisposable.Dispose()
extern void U3CSaveU3Ed__50_System_IDisposable_Dispose_m996D612E7972A9BE67AFDEBB335530863B371CE0 ();
// 0x0000005C System.Boolean NativeToolkit_<Save>d__50::MoveNext()
extern void U3CSaveU3Ed__50_MoveNext_m341E10207D90D2BA8025C2B56B486B5E33A9ABD8 ();
// 0x0000005D System.Object NativeToolkit_<Save>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BCBBECF3C850ACA28D055723BACEC357C6D2FBB ();
// 0x0000005E System.Object NativeToolkit_<Save>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CSaveU3Ed__50_System_Collections_IEnumerator_get_Current_mF80E4736D0867725ADD8F8F71B23D94583972F2D ();
// 0x0000005F System.Void NativeToolkit_<Wait>d__72::.ctor(System.Int32)
extern void U3CWaitU3Ed__72__ctor_m40E5E33029E4AD561A968A9C6C24E64DC726364D ();
// 0x00000060 System.Void NativeToolkit_<Wait>d__72::System.IDisposable.Dispose()
extern void U3CWaitU3Ed__72_System_IDisposable_Dispose_m2A45DF3A73003B3BECD49C5448B37D741690F060 ();
// 0x00000061 System.Boolean NativeToolkit_<Wait>d__72::MoveNext()
extern void U3CWaitU3Ed__72_MoveNext_m36998F0292006514616BCB2BA3D570E90930C635 ();
// 0x00000062 System.Object NativeToolkit_<Wait>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9736A8812546C33B0FFD60FA331952DAF3A01AF5 ();
// 0x00000063 System.Object NativeToolkit_<Wait>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CWaitU3Ed__72_System_Collections_IEnumerator_get_Current_m98CE87B92653BEBA1BA867FC3206656B71AF3B5B ();
// 0x00000064 System.Single AngleCalculator::AngleTwoPoint(UnityEngine.Transform,UnityEngine.Transform)
extern void AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9 ();
// 0x00000065 System.Single AngleCalculator::AngleThreePoint(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
extern void AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF ();
// 0x00000066 System.Void AngleCalculator::DrawLine(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.Single)
extern void AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7 ();
// 0x00000067 System.Void AngleCalculator::.ctor()
extern void AngleCalculator__ctor_m845B37C4E93EA0E798B32995421F0E8E8D816E54 ();
// 0x00000068 System.Void CameraIOS::Start()
extern void CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770 ();
// 0x00000069 System.Void CameraIOS::OnEnable()
extern void CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D ();
// 0x0000006A System.Void CameraIOS::OnDisable()
extern void CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC ();
// 0x0000006B System.Void CameraIOS::OnSaveScreenshotPress()
extern void CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB ();
// 0x0000006C System.Void CameraIOS::OnCameraPress()
extern void CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07 ();
// 0x0000006D System.Void CameraIOS::OnPickImagePress()
extern void CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01 ();
// 0x0000006E System.Void CameraIOS::ScreenshotSaved(System.String)
extern void CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD ();
// 0x0000006F System.Void CameraIOS::CameraShotComplete(UnityEngine.Texture2D,System.String)
extern void CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF ();
// 0x00000070 System.Void CameraIOS::ImagePicked(UnityEngine.Texture2D,System.String)
extern void CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE ();
// 0x00000071 System.Void CameraIOS::.ctor()
extern void CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE ();
// 0x00000072 System.Void DragMouse::Start()
extern void DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663 ();
// 0x00000073 System.Void DragMouse::OnMouseDrag()
extern void DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7 ();
// 0x00000074 System.Void DragMouse::.ctor()
extern void DragMouse__ctor_m50A6B2A45958A4A88B55841255E8B1CDCBC7C462 ();
// 0x00000075 System.Void GameManager::Update()
extern void GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB ();
// 0x00000076 System.Void GameManager::EnabledMode(System.Int32)
extern void GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231 ();
// 0x00000077 System.Void GameManager::.ctor()
extern void GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358 ();
// 0x00000078 System.Void ManagerUI::OpenScene(System.Int32)
extern void ManagerUI_OpenScene_m5BA6194AFD241A7EE0D921E7CEBE0B55886B6604 ();
// 0x00000079 System.Void ManagerUI::AngleMode(System.Int32)
extern void ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E ();
// 0x0000007A System.Void ManagerUI::.ctor()
extern void ManagerUI__ctor_m80A5DD6AE73EA6774FA54E8107C66D9204E5DF50 ();
// 0x0000007B System.Object MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m70DFEBE5F22FB1BA3C9109E3B175FE50C5427CD6 ();
// 0x0000007C System.Boolean MiniJSON.Json_Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mEE1F26C6F6F29820AC03628FEBF2FCD98C8E9981 ();
// 0x0000007D System.Void MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_mD4F79D99F000573D0A99503E9D91BAB25F83C4C6 ();
// 0x0000007E System.Object MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_mEBAB9251E4BF66B008A4BDF328412D3D587DBA7A ();
// 0x0000007F System.Void MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_mC63AE27DE8AC731EF4A7E174D79C7DF5DDE68609 ();
// 0x00000080 System.Collections.Generic.Dictionary`2<System.String,System.Object> MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_m4D199B8E0EE3A3AB813FAA3A31FD7BC54483722C ();
// 0x00000081 System.Collections.Generic.List`1<System.Object> MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_mBB2A7CF5670BC7939C46679BFAB697E48BBDF425 ();
// 0x00000082 System.Object MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_mF8FC1474D6C716FF1C2369B4F4B8E1801D9B237F ();
// 0x00000083 System.Object MiniJSON.Json_Parser::ParseByToken(MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m8A2408323FAA94D6CB80CDA7CA2FE49AFC94C8C4 ();
// 0x00000084 System.String MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_m793142DF0E17E2D24E76B842BA3AC4AD27130F24 ();
// 0x00000085 System.Object MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_mDB86C8DB2B6C67985ED0C0F710F5459C3E523BE5 ();
// 0x00000086 System.Void MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_m1A1EED6D553644B8A3C5534AC3D1335E7202D870 ();
// 0x00000087 System.Char MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_m733ADB33137D592673B090987C13441AF6F705CF ();
// 0x00000088 System.Char MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_mCE145529FA415186E43D9D8F39966210457D8383 ();
// 0x00000089 System.String MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_mDDD0BDAD18222FAC70C3820DF1CA254AE49DEB68 ();
// 0x0000008A MiniJSON.Json_Parser_TOKEN MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m64F6EBFA85C081E6B4D16D05FFA7746D1A86E211 ();
static Il2CppMethodPointer s_methodPointers[138] = 
{
	NativeToolkitExample_Start_m417A512966991FE9BD602AB1AD3FDA51A8872CFF,
	NativeToolkitExample_OnEnable_mBF6629566DF71598D43DF70119FA5751DA0A6CFC,
	NativeToolkitExample_OnDisable_m244A01917182A9D39CDECE9E1DD8315EFE3CCB37,
	NativeToolkitExample_OnSaveScreenshotPress_m5C7808E82FC15D08FF68F350E7192E8627047927,
	NativeToolkitExample_OnSaveImagePress_mB037A647D51D3CA33E54DFF28DA00EEFEAC410AE,
	NativeToolkitExample_OnPickImagePress_m1685B410E7BD84EC4C7B1C766E21C9204C00677B,
	NativeToolkitExample_OnEmailSharePress_m9E9CFF161CDBF0A8135FF12DC542151EF033950F,
	NativeToolkitExample_OnCameraPress_m0FB310F933339F97A983A3056F1F1008C531A467,
	NativeToolkitExample_OnPickContactPress_mDB1965E2B7AE0A06E04643825660B70D313E718E,
	NativeToolkitExample_OnShowAlertPress_m419BDEC5A279AFE357B24DD234A2AB2AE1B7B692,
	NativeToolkitExample_OnShowDialogPress_m1B0473A0B9D6018AC0332A56B3A7BF250F19D0B0,
	NativeToolkitExample_OnLocalNotificationPress_mB01E28C07BA5134FA3B8573AD68F71AF041B44D2,
	NativeToolkitExample_OnClearNotificationsPress_mECB8C2D882A7677373D3E52064E78311F6EDB89A,
	NativeToolkitExample_OnGetLocationPress_mC0A35B1CC168462899A80F622C5DC66FD70355EF,
	NativeToolkitExample_OnRateAppPress_m65445D8B6923D5D96B2D14D929F2230F28394708,
	NativeToolkitExample_ScreenshotSaved_m32D93E731DED22430E69E2EC47E36436DF2F54F0,
	NativeToolkitExample_ImageSaved_m1F06EB47F0365F44B8063F6E63D0B676D1DD29FF,
	NativeToolkitExample_ImagePicked_mD6B09B18C51462B56D98A627A4CDA61C422FA7BA,
	NativeToolkitExample_CameraShotComplete_m1079DCA170B7E6BD6437201AFD6B057BAF9BA213,
	NativeToolkitExample_DialogFinished_m7912810CE75BBD6B22207192FCA6D4CB85F640DC,
	NativeToolkitExample_AppRated_m560C620EF469CDE5B3177A420663B1BF94C55681,
	NativeToolkitExample_ContactPicked_m7DC34FF9EDEBDDAB618AE99488EA4E5806AAA6D5,
	NativeToolkitExample__ctor_m4804840DF1188B930DCE48441CA7F20F04C29FF9,
	NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4,
	NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1,
	NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352,
	NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F,
	NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3,
	NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35,
	NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE,
	NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476,
	NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC,
	NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC,
	NativeToolkit_add_OnRateComplete_m89513DC2DA5391CBEF6AE5D11A6B98C571BB8AEF,
	NativeToolkit_remove_OnRateComplete_mDC0ED5D7A6E141B8E85D998D1B793F942C58E7CD,
	NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C,
	NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE,
	NativeToolkit_add_OnContactPicked_mA74824E31F48D3F5724233AC91597C6C3D05F60F,
	NativeToolkit_remove_OnContactPicked_mC2B069F3D41DCBCD15D27F617237DDCCA21DBB96,
	NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC,
	NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754,
	NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7,
	NativeToolkit_pickContact_m5B52D7B975AF656010B1DF95DEEA8EE14CA5DCF1,
	NativeToolkit_getLocale_m0426AF05D41E201B6B0CAD94BC52DE038FA4ED8F,
	NativeToolkit_sendEmail_m896C9771723C12700898D7965518D70CDC926906,
	NativeToolkit_scheduleLocalNotification_m7D10ED770DEC3DC3AE18F047A901E93A8A923051,
	NativeToolkit_clearLocalNotification_m8FE0A206C3C3C82BDCAF608B86E396E5B4D6D617,
	NativeToolkit_clearAllLocalNotifications_m65CC6A7D6F8456AE5754D0F0BF1166AC073E76A7,
	NativeToolkit_wasLaunchedFromNotification_m69282D5647DB107B0C6B9E501383C3747FC8C93F,
	NativeToolkit_rateApp_m59D35367851F9C075F418E88C7734172AE335BE3,
	NativeToolkit_showConfirm_m8D3BC147FF343E24EA877EB6E87B58BD087F5EFE,
	NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B,
	NativeToolkit_startLocation_mEE1F8F4801DBFB5C11678245C442A94EF72585B5,
	NativeToolkit_getLongitude_m7BF826B2F4917F85111E0EEE1282426E8BE12267,
	NativeToolkit_getLatitude_m7FA134FC8257BB863AE140C5F6486ADC1A75D5BE,
	NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6,
	NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76,
	NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB,
	NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA,
	NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD,
	NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F,
	NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE,
	NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30,
	NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E,
	NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC,
	NativeToolkit_PickContact_m3B22F28B543CC7CD91342C0CE3F87899F910981C,
	NativeToolkit_OnPickContactFinished_m464156AA1AC55C69C053B107985F5BC8ECC33355,
	NativeToolkit_SendEmail_m25AA52AD39886B040D106E2EE9BE9CB09F71BC08,
	NativeToolkit_ShowConfirm_mBDCCC1703CB6F049BA940FDB4EDB46808525A8E4,
	NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2,
	NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E,
	NativeToolkit_RateApp_mC01ADAD62E4F721F7B69DC5C0D734A717D242E7D,
	NativeToolkit_OnRatePress_m57BA5B69E0696927C09EAA16F9B07C2653561346,
	NativeToolkit_StartLocation_m8FCA45DADC62B70F074D78FC8AED57281408BA94,
	NativeToolkit_GetLongitude_mA8F0E6D341CA0F3B2A2948F48120C68C0E43870A,
	NativeToolkit_GetLatitude_m704B406D3C7F0E972880131CE1F4FD5DD4802EB6,
	NativeToolkit_GetCountryCode_mB2D3A5D2EC2B8AC9EA762BE0861C8825CB4E7D59,
	NativeToolkit_ScheduleLocalNotification_m915F92A1B2398362C99A9D1E1259F329A5B3242C,
	NativeToolkit_ClearLocalNotification_mC69F2CD75F6ED7F7DCFBB5CB38BA47BB8B994A2C,
	NativeToolkit_ClearAllLocalNotifications_m6D9FDEF752C97B9F4E5E36D57EDBF9B2A221A628,
	NativeToolkit_WasLaunchedFromNotification_mD48C7B6A2C9A0B77F7FA33413FE29B144F8ABC37,
	NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0,
	NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE,
	NativeToolkit__ctor_m385E36B7410BADB983BB42E07067986E46F3CEFC,
	U3CGrabScreenshotU3Ed__48__ctor_m996D01A5467FC6C4255E693A511C813583CE00DE,
	U3CGrabScreenshotU3Ed__48_System_IDisposable_Dispose_m5AE7CDEADC6959C94862E54FFACA3AF795BB8DFF,
	U3CGrabScreenshotU3Ed__48_MoveNext_m4C317CD3C2E805CDBF675DE7B2F9813C8EF4E775,
	U3CGrabScreenshotU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB50117431D009B01F608DF2D02058F83119BDDC7,
	U3CGrabScreenshotU3Ed__48_System_Collections_IEnumerator_get_Current_m3082F429511212FCF2C16A510CA84A955EDD988B,
	U3CSaveU3Ed__50__ctor_m0CAD84EBC0DF895828EF24C9FE2B45EBFB3E9530,
	U3CSaveU3Ed__50_System_IDisposable_Dispose_m996D612E7972A9BE67AFDEBB335530863B371CE0,
	U3CSaveU3Ed__50_MoveNext_m341E10207D90D2BA8025C2B56B486B5E33A9ABD8,
	U3CSaveU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BCBBECF3C850ACA28D055723BACEC357C6D2FBB,
	U3CSaveU3Ed__50_System_Collections_IEnumerator_get_Current_mF80E4736D0867725ADD8F8F71B23D94583972F2D,
	U3CWaitU3Ed__72__ctor_m40E5E33029E4AD561A968A9C6C24E64DC726364D,
	U3CWaitU3Ed__72_System_IDisposable_Dispose_m2A45DF3A73003B3BECD49C5448B37D741690F060,
	U3CWaitU3Ed__72_MoveNext_m36998F0292006514616BCB2BA3D570E90930C635,
	U3CWaitU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9736A8812546C33B0FFD60FA331952DAF3A01AF5,
	U3CWaitU3Ed__72_System_Collections_IEnumerator_get_Current_m98CE87B92653BEBA1BA867FC3206656B71AF3B5B,
	AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9,
	AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF,
	AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7,
	AngleCalculator__ctor_m845B37C4E93EA0E798B32995421F0E8E8D816E54,
	CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770,
	CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D,
	CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC,
	CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB,
	CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07,
	CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01,
	CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD,
	CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF,
	CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE,
	CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE,
	DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663,
	DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7,
	DragMouse__ctor_m50A6B2A45958A4A88B55841255E8B1CDCBC7C462,
	GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB,
	GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231,
	GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358,
	ManagerUI_OpenScene_m5BA6194AFD241A7EE0D921E7CEBE0B55886B6604,
	ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E,
	ManagerUI__ctor_m80A5DD6AE73EA6774FA54E8107C66D9204E5DF50,
	Json_Deserialize_m70DFEBE5F22FB1BA3C9109E3B175FE50C5427CD6,
	Parser_IsWordBreak_mEE1F26C6F6F29820AC03628FEBF2FCD98C8E9981,
	Parser__ctor_mD4F79D99F000573D0A99503E9D91BAB25F83C4C6,
	Parser_Parse_mEBAB9251E4BF66B008A4BDF328412D3D587DBA7A,
	Parser_Dispose_mC63AE27DE8AC731EF4A7E174D79C7DF5DDE68609,
	Parser_ParseObject_m4D199B8E0EE3A3AB813FAA3A31FD7BC54483722C,
	Parser_ParseArray_mBB2A7CF5670BC7939C46679BFAB697E48BBDF425,
	Parser_ParseValue_mF8FC1474D6C716FF1C2369B4F4B8E1801D9B237F,
	Parser_ParseByToken_m8A2408323FAA94D6CB80CDA7CA2FE49AFC94C8C4,
	Parser_ParseString_m793142DF0E17E2D24E76B842BA3AC4AD27130F24,
	Parser_ParseNumber_mDB86C8DB2B6C67985ED0C0F710F5459C3E523BE5,
	Parser_EatWhitespace_m1A1EED6D553644B8A3C5534AC3D1335E7202D870,
	Parser_get_PeekChar_m733ADB33137D592673B090987C13441AF6F705CF,
	Parser_get_NextChar_mCE145529FA415186E43D9D8F39966210457D8383,
	Parser_get_NextWord_mDDD0BDAD18222FAC70C3820DF1CA254AE49DEB68,
	Parser_get_NextToken_m64F6EBFA85C081E6B4D16D05FFA7746D1A86E211,
};
static const int32_t s_InvokerIndices[138] = 
{
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	22,
	22,
	30,
	30,
	27,
	22,
	128,
	19,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	276,
	91,
	3,
	3,
	3,
	4,
	1347,
	1348,
	276,
	3,
	132,
	1347,
	1349,
	516,
	3,
	1350,
	1350,
	4,
	19,
	1351,
	1352,
	516,
	378,
	3,
	22,
	3,
	22,
	3,
	22,
	1347,
	1353,
	1349,
	22,
	1354,
	22,
	132,
	1350,
	1350,
	4,
	1355,
	105,
	3,
	132,
	0,
	1262,
	19,
	28,
	19,
	104,
	11,
	11,
	28,
	19,
	104,
	11,
	11,
	28,
	19,
	104,
	11,
	11,
	191,
	1356,
	1357,
	19,
	19,
	19,
	19,
	19,
	19,
	19,
	22,
	30,
	30,
	19,
	19,
	19,
	19,
	19,
	28,
	19,
	28,
	28,
	19,
	0,
	45,
	22,
	0,
	19,
	11,
	11,
	11,
	31,
	11,
	11,
	19,
	165,
	165,
	11,
	7,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	138,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
