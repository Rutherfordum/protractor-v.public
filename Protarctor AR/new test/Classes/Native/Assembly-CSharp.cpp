﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// AngleCalculator
struct AngleCalculator_tDACEF6B6E9AA9576D1B11E23FC4306DEA31388C2;
// CameraIOS
struct CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596;
// DragMouse
struct DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A;
// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89;
// ManagerUI
struct ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE;
// NativeToolkit
struct NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F;
// NativeToolkit/<GrabScreenshot>d__30
struct U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5;
// NativeToolkit/<Save>d__32
struct U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD;
// NativeToolkit/<Wait>d__40
struct U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Action`2<UnityEngine.Texture2D,System.String>
struct Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00DB7105B91D9A97273A38086D23BE5522570D93;
IL2CPP_EXTERN_C String_t* _stringLiteral14993722CFD06F0E85FCC82A9DC489400BEEABB9;
IL2CPP_EXTERN_C String_t* _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0;
IL2CPP_EXTERN_C String_t* _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF;
IL2CPP_EXTERN_C String_t* _stringLiteral4018E8848406C17CAE50BFB035BBE7F8410CF52B;
IL2CPP_EXTERN_C String_t* _stringLiteral406ADB4AA806CB511098281A7FF1113D10B10ACA;
IL2CPP_EXTERN_C String_t* _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8;
IL2CPP_EXTERN_C String_t* _stringLiteral4FB88D19D18BCD0C313F5FC4A552B4A8CC64282F;
IL2CPP_EXTERN_C String_t* _stringLiteral5397E0583F14F6C88DE06B1EF28F460A1FB5B0AE;
IL2CPP_EXTERN_C String_t* _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787;
IL2CPP_EXTERN_C String_t* _stringLiteral55D0C1BB201EC0F84A70A5B21BEE3C1BEDD44749;
IL2CPP_EXTERN_C String_t* _stringLiteral69B1910FDC4121E685DFD6A556FC93F1F8206505;
IL2CPP_EXTERN_C String_t* _stringLiteral6EC6121CD150A73105F36A36219F4E978FE2E3DC;
IL2CPP_EXTERN_C String_t* _stringLiteral816C52FD2BDD94A63CD0944823A6C0AA9384C103;
IL2CPP_EXTERN_C String_t* _stringLiteral9040A7D6CDF7A0D6CAB1823831C6CEB7D01AF97F;
IL2CPP_EXTERN_C String_t* _stringLiteral96D7BA069613DE9BA657F01A2C8471BC0DE8C407;
IL2CPP_EXTERN_C String_t* _stringLiteral9B9AEF62C4AAC4568E56105AE46134B32107BBDB;
IL2CPP_EXTERN_C String_t* _stringLiteralA041727F44C06126C7D6079E6FEB153B861215C3;
IL2CPP_EXTERN_C String_t* _stringLiteralA1BF92EFF40DD4ADEA2328F9E0F7C938AE031DEC;
IL2CPP_EXTERN_C String_t* _stringLiteralB0A98216A32426B9E66A4AC1EB6DF2E96E1B495C;
IL2CPP_EXTERN_C String_t* _stringLiteralB22D24206653FBF96FAD4AFEB724C4A2C2FDC933;
IL2CPP_EXTERN_C String_t* _stringLiteralD521A4D20A49C36E58B0788F07F2834210EC2C07;
IL2CPP_EXTERN_C String_t* _stringLiteralDCDFD41CB6E96E78736DEB5F5ADD6F6311DA8E80;
IL2CPP_EXTERN_C String_t* _stringLiteralE768FFA35BEFAEE87A2CB4ED20194AAFD3A43C5C;
IL2CPP_EXTERN_C String_t* _stringLiteralEFFC8EA4C80645993C7BBF3A87CEEF006DD7F41D;
IL2CPP_EXTERN_C String_t* _stringLiteralF04CAE694D5225DFD7F7A62D33B7CD4B90717134;
IL2CPP_EXTERN_C String_t* _stringLiteralF942D1239DAD74CB19F1D94C6877FF651D007775;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m4FDEC794781FBC6B576389503CE9A9B7ED1A06AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisNativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_mA461E7FFC6C262734D7D72C67BD8FCC3F351020A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m807C070DCBD260C48DCA9A6267F38EF36112604A_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CGrabScreenshotU3Ed__30_MoveNext_m988CDF05BFAC0D86EA8E632E17B54DC33E28FC7C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CSaveU3Ed__32_MoveNext_mE2C978BE5590BFF0D016F75D9DD24AAB8CB566ED_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// NativeToolkit_<Wait>d__40
struct  U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Wait>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Wait>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single NativeToolkit_<Wait>d__40::delay
	float ___delay_2;
	// System.Single NativeToolkit_<Wait>d__40::<pauseTarget>5__2
	float ___U3CpauseTargetU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CpauseTargetU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8, ___U3CpauseTargetU3E5__2_3)); }
	inline float get_U3CpauseTargetU3E5__2_3() const { return ___U3CpauseTargetU3E5__2_3; }
	inline float* get_address_of_U3CpauseTargetU3E5__2_3() { return &___U3CpauseTargetU3E5__2_3; }
	inline void set_U3CpauseTargetU3E5__2_3(float value)
	{
		___U3CpauseTargetU3E5__2_3 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:

public:
};


// NativeToolkit_<GrabScreenshot>d__30
struct  U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<GrabScreenshot>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<GrabScreenshot>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Rect NativeToolkit_<GrabScreenshot>d__30::screenArea
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea_2;
	// System.String NativeToolkit_<GrabScreenshot>d__30::fileType
	String_t* ___fileType_3;
	// System.String NativeToolkit_<GrabScreenshot>d__30::fileName
	String_t* ___fileName_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_screenArea_2() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___screenArea_2)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_screenArea_2() const { return ___screenArea_2; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_screenArea_2() { return &___screenArea_2; }
	inline void set_screenArea_2(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___screenArea_2 = value;
	}

	inline static int32_t get_offset_of_fileType_3() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___fileType_3)); }
	inline String_t* get_fileType_3() const { return ___fileType_3; }
	inline String_t** get_address_of_fileType_3() { return &___fileType_3; }
	inline void set_fileType_3(String_t* value)
	{
		___fileType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileType_3), (void*)value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileName_4), (void*)value);
	}
};


// NativeToolkit_ImageType
struct  ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130 
{
public:
	// System.Int32 NativeToolkit_ImageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NativeToolkit_SaveStatus
struct  SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC 
{
public:
	// System.Int32 NativeToolkit_SaveStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NativeToolkit_<Save>d__32
struct  U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Save>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Save>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String NativeToolkit_<Save>d__32::path
	String_t* ___path_2;
	// System.Byte[] NativeToolkit_<Save>d__32::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// NativeToolkit_ImageType NativeToolkit_<Save>d__32::imageType
	int32_t ___imageType_4;
	// System.Int32 NativeToolkit_<Save>d__32::<count>5__2
	int32_t ___U3CcountU3E5__2_5;
	// NativeToolkit_SaveStatus NativeToolkit_<Save>d__32::<saved>5__3
	int32_t ___U3CsavedU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_2), (void*)value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_imageType_4() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___imageType_4)); }
	inline int32_t get_imageType_4() const { return ___imageType_4; }
	inline int32_t* get_address_of_imageType_4() { return &___imageType_4; }
	inline void set_imageType_4(int32_t value)
	{
		___imageType_4 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CcountU3E5__2_5)); }
	inline int32_t get_U3CcountU3E5__2_5() const { return ___U3CcountU3E5__2_5; }
	inline int32_t* get_address_of_U3CcountU3E5__2_5() { return &___U3CcountU3E5__2_5; }
	inline void set_U3CcountU3E5__2_5(int32_t value)
	{
		___U3CcountU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CsavedU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD, ___U3CsavedU3E5__3_6)); }
	inline int32_t get_U3CsavedU3E5__3_6() const { return ___U3CsavedU3E5__3_6; }
	inline int32_t* get_address_of_U3CsavedU3E5__3_6() { return &___U3CsavedU3E5__3_6; }
	inline void set_U3CsavedU3E5__3_6(int32_t value)
	{
		___U3CsavedU3E5__3_6 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.Action`1<System.Boolean>
struct  Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.String>
struct  Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<UnityEngine.Texture2D>
struct  Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<UnityEngine.Texture2D,System.String>
struct  Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// AngleCalculator
struct  AngleCalculator_tDACEF6B6E9AA9576D1B11E23FC4306DEA31388C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// CameraIOS
struct  CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image CameraIOS::Photo
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Photo_4;
	// System.String CameraIOS::Activate
	String_t* ___Activate_5;

public:
	inline static int32_t get_offset_of_Photo_4() { return static_cast<int32_t>(offsetof(CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596, ___Photo_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Photo_4() const { return ___Photo_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Photo_4() { return &___Photo_4; }
	inline void set_Photo_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Photo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Photo_4), (void*)value);
	}

	inline static int32_t get_offset_of_Activate_5() { return static_cast<int32_t>(offsetof(CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596, ___Activate_5)); }
	inline String_t* get_Activate_5() const { return ___Activate_5; }
	inline String_t** get_address_of_Activate_5() { return &___Activate_5; }
	inline void set_Activate_5(String_t* value)
	{
		___Activate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Activate_5), (void*)value);
	}
};


// DragMouse
struct  DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera DragMouse::MainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___MainCamera_4;
	// UnityEngine.Vector3 DragMouse::poseInput
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___poseInput_5;
	// UnityEngine.Vector3 DragMouse::poseTarget
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___poseTarget_6;
	// System.Single DragMouse::z
	float ___z_7;
	// System.Int32 DragMouse::id
	int32_t ___id_9;

public:
	inline static int32_t get_offset_of_MainCamera_4() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___MainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_MainCamera_4() const { return ___MainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_MainCamera_4() { return &___MainCamera_4; }
	inline void set_MainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___MainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_poseInput_5() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___poseInput_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_poseInput_5() const { return ___poseInput_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_poseInput_5() { return &___poseInput_5; }
	inline void set_poseInput_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___poseInput_5 = value;
	}

	inline static int32_t get_offset_of_poseTarget_6() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___poseTarget_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_poseTarget_6() const { return ___poseTarget_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_poseTarget_6() { return &___poseTarget_6; }
	inline void set_poseTarget_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___poseTarget_6 = value;
	}

	inline static int32_t get_offset_of_z_7() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___z_7)); }
	inline float get_z_7() const { return ___z_7; }
	inline float* get_address_of_z_7() { return &___z_7; }
	inline void set_z_7(float value)
	{
		___z_7 = value;
	}

	inline static int32_t get_offset_of_id_9() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A, ___id_9)); }
	inline int32_t get_id_9() const { return ___id_9; }
	inline int32_t* get_address_of_id_9() { return &___id_9; }
	inline void set_id_9(int32_t value)
	{
		___id_9 = value;
	}
};

struct DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields
{
public:
	// System.Int32 DragMouse::idPoint
	int32_t ___idPoint_8;

public:
	inline static int32_t get_offset_of_idPoint_8() { return static_cast<int32_t>(offsetof(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields, ___idPoint_8)); }
	inline int32_t get_idPoint_8() const { return ___idPoint_8; }
	inline int32_t* get_address_of_idPoint_8() { return &___idPoint_8; }
	inline void set_idPoint_8(int32_t value)
	{
		___idPoint_8 = value;
	}
};


// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text GameManager::angleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___angleText_4;
	// UnityEngine.Transform GameManager::main
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main_5;
	// UnityEngine.Transform GameManager::one
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one_6;
	// UnityEngine.Transform GameManager::two
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two_7;
	// UnityEngine.Transform GameManager::lineOne
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lineOne_8;
	// UnityEngine.Transform GameManager::lineTwo
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lineTwo_9;
	// UnityEngine.Transform GameManager::three
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___three_10;
	// UnityEngine.Transform GameManager::four
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___four_11;

public:
	inline static int32_t get_offset_of_angleText_4() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___angleText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_angleText_4() const { return ___angleText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_angleText_4() { return &___angleText_4; }
	inline void set_angleText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___angleText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___angleText_4), (void*)value);
	}

	inline static int32_t get_offset_of_main_5() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___main_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_main_5() const { return ___main_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_main_5() { return &___main_5; }
	inline void set_main_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___main_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___main_5), (void*)value);
	}

	inline static int32_t get_offset_of_one_6() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___one_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_one_6() const { return ___one_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_one_6() { return &___one_6; }
	inline void set_one_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___one_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___one_6), (void*)value);
	}

	inline static int32_t get_offset_of_two_7() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___two_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_two_7() const { return ___two_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_two_7() { return &___two_7; }
	inline void set_two_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___two_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___two_7), (void*)value);
	}

	inline static int32_t get_offset_of_lineOne_8() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___lineOne_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lineOne_8() const { return ___lineOne_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lineOne_8() { return &___lineOne_8; }
	inline void set_lineOne_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lineOne_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineOne_8), (void*)value);
	}

	inline static int32_t get_offset_of_lineTwo_9() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___lineTwo_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lineTwo_9() const { return ___lineTwo_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lineTwo_9() { return &___lineTwo_9; }
	inline void set_lineTwo_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lineTwo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lineTwo_9), (void*)value);
	}

	inline static int32_t get_offset_of_three_10() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___three_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_three_10() const { return ___three_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_three_10() { return &___three_10; }
	inline void set_three_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___three_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___three_10), (void*)value);
	}

	inline static int32_t get_offset_of_four_11() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___four_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_four_11() const { return ___four_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_four_11() { return &___four_11; }
	inline void set_four_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___four_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___four_11), (void*)value);
	}
};


// ManagerUI
struct  ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields
{
public:
	// System.Int32 ManagerUI::ModeAngle
	int32_t ___ModeAngle_4;

public:
	inline static int32_t get_offset_of_ModeAngle_4() { return static_cast<int32_t>(offsetof(ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields, ___ModeAngle_4)); }
	inline int32_t get_ModeAngle_4() const { return ___ModeAngle_4; }
	inline int32_t* get_address_of_ModeAngle_4() { return &___ModeAngle_4; }
	inline void set_ModeAngle_4(int32_t value)
	{
		___ModeAngle_4 = value;
	}
};


// NativeToolkit
struct  NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields
{
public:
	// System.Action`1<UnityEngine.Texture2D> NativeToolkit::OnScreenshotTaken
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * ___OnScreenshotTaken_4;
	// System.Action`1<System.String> NativeToolkit::OnScreenshotSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnScreenshotSaved_5;
	// System.Action`1<System.String> NativeToolkit::OnImageSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnImageSaved_6;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnImagePicked
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___OnImagePicked_7;
	// System.Action`1<System.Boolean> NativeToolkit::OnDialogComplete
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___OnDialogComplete_8;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnCameraShotComplete
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___OnCameraShotComplete_9;
	// NativeToolkit NativeToolkit::instance
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * ___instance_10;
	// UnityEngine.GameObject NativeToolkit::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_11;

public:
	inline static int32_t get_offset_of_OnScreenshotTaken_4() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotTaken_4)); }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * get_OnScreenshotTaken_4() const { return ___OnScreenshotTaken_4; }
	inline Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 ** get_address_of_OnScreenshotTaken_4() { return &___OnScreenshotTaken_4; }
	inline void set_OnScreenshotTaken_4(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * value)
	{
		___OnScreenshotTaken_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnScreenshotTaken_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnScreenshotSaved_5() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotSaved_5)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnScreenshotSaved_5() const { return ___OnScreenshotSaved_5; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnScreenshotSaved_5() { return &___OnScreenshotSaved_5; }
	inline void set_OnScreenshotSaved_5(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnScreenshotSaved_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnScreenshotSaved_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnImageSaved_6() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImageSaved_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnImageSaved_6() const { return ___OnImageSaved_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnImageSaved_6() { return &___OnImageSaved_6; }
	inline void set_OnImageSaved_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnImageSaved_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnImageSaved_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnImagePicked_7() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImagePicked_7)); }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * get_OnImagePicked_7() const { return ___OnImagePicked_7; }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 ** get_address_of_OnImagePicked_7() { return &___OnImagePicked_7; }
	inline void set_OnImagePicked_7(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * value)
	{
		___OnImagePicked_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnImagePicked_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnDialogComplete_8() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnDialogComplete_8)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_OnDialogComplete_8() const { return ___OnDialogComplete_8; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_OnDialogComplete_8() { return &___OnDialogComplete_8; }
	inline void set_OnDialogComplete_8(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___OnDialogComplete_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDialogComplete_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnCameraShotComplete_9() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnCameraShotComplete_9)); }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * get_OnCameraShotComplete_9() const { return ___OnCameraShotComplete_9; }
	inline Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 ** get_address_of_OnCameraShotComplete_9() { return &___OnCameraShotComplete_9; }
	inline void set_OnCameraShotComplete_9(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * value)
	{
		___OnCameraShotComplete_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnCameraShotComplete_9), (void*)value);
	}

	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___instance_10)); }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * get_instance_10() const { return ___instance_10; }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_10), (void*)value);
	}

	inline static int32_t get_offset_of_go_11() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___go_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_11() const { return ___go_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_11() { return &___go_11; }
	inline void set_go_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___go_11), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_29;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_31;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_32;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_33;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_29)); }
	inline bool get_m_IncludeForMasking_29() const { return ___m_IncludeForMasking_29; }
	inline bool* get_address_of_m_IncludeForMasking_29() { return &___m_IncludeForMasking_29; }
	inline void set_m_IncludeForMasking_29(bool value)
	{
		___m_IncludeForMasking_29 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_30)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_30() const { return ___m_OnCullStateChanged_30; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_30() { return &___m_OnCullStateChanged_30; }
	inline void set_m_OnCullStateChanged_30(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_31)); }
	inline bool get_m_ShouldRecalculate_31() const { return ___m_ShouldRecalculate_31; }
	inline bool* get_address_of_m_ShouldRecalculate_31() { return &___m_ShouldRecalculate_31; }
	inline void set_m_ShouldRecalculate_31(bool value)
	{
		___m_ShouldRecalculate_31 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_32)); }
	inline int32_t get_m_StencilValue_32() const { return ___m_StencilValue_32; }
	inline int32_t* get_address_of_m_StencilValue_32() { return &___m_StencilValue_32; }
	inline void set_m_StencilValue_32(int32_t value)
	{
		___m_StencilValue_32 = value;
	}

	inline static int32_t get_offset_of_m_Corners_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_33)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_33() const { return ___m_Corners_33; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_33() { return &___m_Corners_33; }
	inline void set_m_Corners_33(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_33), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_35;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_36;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_37;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_38;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_39;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_40;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_41;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_42;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_43;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_44;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_45;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_46;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_47;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_48;

public:
	inline static int32_t get_offset_of_m_Sprite_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_35() const { return ___m_Sprite_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_35() { return &___m_Sprite_35; }
	inline void set_m_Sprite_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_36)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_36() const { return ___m_OverrideSprite_36; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_36() { return &___m_OverrideSprite_36; }
	inline void set_m_OverrideSprite_36(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_37)); }
	inline int32_t get_m_Type_37() const { return ___m_Type_37; }
	inline int32_t* get_address_of_m_Type_37() { return &___m_Type_37; }
	inline void set_m_Type_37(int32_t value)
	{
		___m_Type_37 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_38)); }
	inline bool get_m_PreserveAspect_38() const { return ___m_PreserveAspect_38; }
	inline bool* get_address_of_m_PreserveAspect_38() { return &___m_PreserveAspect_38; }
	inline void set_m_PreserveAspect_38(bool value)
	{
		___m_PreserveAspect_38 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_39)); }
	inline bool get_m_FillCenter_39() const { return ___m_FillCenter_39; }
	inline bool* get_address_of_m_FillCenter_39() { return &___m_FillCenter_39; }
	inline void set_m_FillCenter_39(bool value)
	{
		___m_FillCenter_39 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_40)); }
	inline int32_t get_m_FillMethod_40() const { return ___m_FillMethod_40; }
	inline int32_t* get_address_of_m_FillMethod_40() { return &___m_FillMethod_40; }
	inline void set_m_FillMethod_40(int32_t value)
	{
		___m_FillMethod_40 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_41)); }
	inline float get_m_FillAmount_41() const { return ___m_FillAmount_41; }
	inline float* get_address_of_m_FillAmount_41() { return &___m_FillAmount_41; }
	inline void set_m_FillAmount_41(float value)
	{
		___m_FillAmount_41 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_42)); }
	inline bool get_m_FillClockwise_42() const { return ___m_FillClockwise_42; }
	inline bool* get_address_of_m_FillClockwise_42() { return &___m_FillClockwise_42; }
	inline void set_m_FillClockwise_42(bool value)
	{
		___m_FillClockwise_42 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_43)); }
	inline int32_t get_m_FillOrigin_43() const { return ___m_FillOrigin_43; }
	inline int32_t* get_address_of_m_FillOrigin_43() { return &___m_FillOrigin_43; }
	inline void set_m_FillOrigin_43(int32_t value)
	{
		___m_FillOrigin_43 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_44)); }
	inline float get_m_AlphaHitTestMinimumThreshold_44() const { return ___m_AlphaHitTestMinimumThreshold_44; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_44() { return &___m_AlphaHitTestMinimumThreshold_44; }
	inline void set_m_AlphaHitTestMinimumThreshold_44(float value)
	{
		___m_AlphaHitTestMinimumThreshold_44 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_45)); }
	inline bool get_m_Tracked_45() const { return ___m_Tracked_45; }
	inline bool* get_address_of_m_Tracked_45() { return &___m_Tracked_45; }
	inline void set_m_Tracked_45(bool value)
	{
		___m_Tracked_45 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_46)); }
	inline bool get_m_UseSpriteMesh_46() const { return ___m_UseSpriteMesh_46; }
	inline bool* get_address_of_m_UseSpriteMesh_46() { return &___m_UseSpriteMesh_46; }
	inline void set_m_UseSpriteMesh_46(bool value)
	{
		___m_UseSpriteMesh_46 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PixelsPerUnitMultiplier_47)); }
	inline float get_m_PixelsPerUnitMultiplier_47() const { return ___m_PixelsPerUnitMultiplier_47; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_47() { return &___m_PixelsPerUnitMultiplier_47; }
	inline void set_m_PixelsPerUnitMultiplier_47(float value)
	{
		___m_PixelsPerUnitMultiplier_47 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_CachedReferencePixelsPerUnit_48)); }
	inline float get_m_CachedReferencePixelsPerUnit_48() const { return ___m_CachedReferencePixelsPerUnit_48; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_48() { return &___m_CachedReferencePixelsPerUnit_48; }
	inline void set_m_CachedReferencePixelsPerUnit_48(float value)
	{
		___m_CachedReferencePixelsPerUnit_48 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_34;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_49;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_50;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_51;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_52;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * ___m_TrackedTexturelessImages_53;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_54;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_34() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_34() const { return ___s_ETC1DefaultUI_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_34() { return &___s_ETC1DefaultUI_34; }
	inline void set_s_ETC1DefaultUI_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_34), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_49)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_49() const { return ___s_VertScratch_49; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_49() { return &___s_VertScratch_49; }
	inline void set_s_VertScratch_49(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_49), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_50() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_50)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_50() const { return ___s_UVScratch_50; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_50() { return &___s_UVScratch_50; }
	inline void set_s_UVScratch_50(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_50), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_51() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_51)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_51() const { return ___s_Xy_51; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_51() { return &___s_Xy_51; }
	inline void set_s_Xy_51(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_52() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_52)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_52() const { return ___s_Uv_52; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_52() { return &___s_Uv_52; }
	inline void set_s_Uv_52(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_52), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_53() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_53)); }
	inline List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * get_m_TrackedTexturelessImages_53() const { return ___m_TrackedTexturelessImages_53; }
	inline List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 ** get_address_of_m_TrackedTexturelessImages_53() { return &___m_TrackedTexturelessImages_53; }
	inline void set_m_TrackedTexturelessImages_53(List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * value)
	{
		___m_TrackedTexturelessImages_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_54() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_54)); }
	inline bool get_s_Initialized_54() const { return ___s_Initialized_54; }
	inline bool* get_address_of_s_Initialized_54() { return &___s_Initialized_54; }
	inline void set_s_Initialized_54(bool value)
	{
		___s_Initialized_54 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_34;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_35;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_37;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_39;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_40;

public:
	inline static int32_t get_offset_of_m_FontData_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_34)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_34() const { return ___m_FontData_34; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_34() { return &___m_FontData_34; }
	inline void set_m_FontData_34(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_35)); }
	inline String_t* get_m_Text_35() const { return ___m_Text_35; }
	inline String_t** get_address_of_m_Text_35() { return &___m_Text_35; }
	inline void set_m_Text_35(String_t* value)
	{
		___m_Text_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_36)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_36() const { return ___m_TextCache_36; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_36() { return &___m_TextCache_36; }
	inline void set_m_TextCache_36(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_37() const { return ___m_TextCacheForLayout_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_37() { return &___m_TextCacheForLayout_37; }
	inline void set_m_TextCacheForLayout_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_39)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_39() const { return ___m_DisableFontTextureRebuiltCallback_39; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_39() { return &___m_DisableFontTextureRebuiltCallback_39; }
	inline void set_m_DisableFontTextureRebuiltCallback_39(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_39 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_40)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_40() const { return ___m_TempVerts_40; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_40() { return &___m_TempVerts_40; }
	inline void set_m_TempVerts_40(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_40), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_38;

public:
	inline static int32_t get_offset_of_s_DefaultText_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_38)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_38() const { return ___s_DefaultText_38; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_38() { return &___s_DefaultText_38; }
	inline void set_s_DefaultText_38(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_38), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mB83B0C1C61CED5B54803D334FFC7187881D32EFB_gshared (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m251F203B12669585A5E663F529F6DF09F931B5DA_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mE053F7A95F30AFF07D69F0DED3DA13AE2EC25E03_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2_Invoke_m1738FFAE74BE5E599FD42520FA2BEF69D1AC4709_gshared (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_gshared (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, bool ___obj0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);

// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Angle_m8911FFA1DD1C8C46D923B52645B352FA1521CD5F (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_forward_m02858E8B3313B27174B19E9113F24EF25FBCEC7F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void CameraIOS::OnPickImagePress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01 (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method);
// System.Void CameraIOS::OnCameraPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07 (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void NativeToolkit::add_OnScreenshotSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void System.Action`2<UnityEngine.Texture2D,System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913 (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_mB83B0C1C61CED5B54803D334FFC7187881D32EFB_gshared)(__this, ___object0, ___method1, method);
}
// System.Void NativeToolkit::add_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method);
// System.Void NativeToolkit::add_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method);
// System.Void NativeToolkit::remove_OnScreenshotSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void NativeToolkit::remove_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method);
// System.Void NativeToolkit::remove_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476 (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method);
// System.Void NativeToolkit::SaveScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB (String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea3, const RuntimeMethod* method);
// System.Void NativeToolkit::TakeCameraShot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E (const RuntimeMethod* method);
// System.Void NativeToolkit::PickImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE (const RuntimeMethod* method);
// System.Void NativeToolkit::ShowAlert(System.String,System.String,System.Action`1<System.Boolean>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2 (String_t* ___title0, String_t* ___message1, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback2, String_t* ___btnText3, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot2, float ___pixelsPerUnit3, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m0D1DBEFD8E361B3967964C71F93EF47E2AC53A13 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Object_FindObjectOfType_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m807C070DCBD260C48DCA9A6267F38EF36112604A (const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m251F203B12669585A5E663F529F6DF09F931B5DA_gshared)(method);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Input_get_mousePosition_m1F6706785983B41FE8D5CBB81B5F15F68EBD9A53 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void GameManager::EnabledMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231 (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * __this, int32_t ___mode0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Single AngleCalculator::AngleThreePoint(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one1, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two2, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE (float* __this, String_t* ___format0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void AngleCalculator::DrawLine(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___obj1, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___line2, float ___sizeLine3, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Single AngleCalculator::AngleTwoPoint(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m84E39F1210BA4482CF2D8A2F068198760DE4B4C5 (int32_t ___sceneBuildIndex0, int32_t ___mode1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<NativeToolkit>()
inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * GameObject_AddComponent_TisNativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_mA461E7FFC6C262734D7D72C67BD8FCC3F351020A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mE053F7A95F30AFF07D69F0DED3DA13AE2EC25E03_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___lhs0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rhs1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// NativeToolkit NativeToolkit::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6 (const RuntimeMethod* method);
// System.Collections.IEnumerator NativeToolkit::GrabScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea3, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void NativeToolkit/<GrabScreenshot>d__30::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGrabScreenshotU3Ed__30__ctor_mE9C27BC1166709434CAF40D4004ABAB4E3E84597 (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void NativeToolkit::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76 (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ImageConversion_EncodeToPNG_m8D67A36A7D81F436CDA108CC5293E15A9CFD5617 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex0, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToJPG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ImageConversion_EncodeToJPG_mDE6C67AACCC7BAD006D0A852093F881D11F9C8DF (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex0, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_persistentDataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B (const RuntimeMethod* method);
// System.Collections.IEnumerator NativeToolkit::Save(System.Byte[],System.String,System.String,NativeToolkit/ImageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes0, String_t* ___fileName1, String_t* ___path2, int32_t ___imageType3, const RuntimeMethod* method);
// System.Void NativeToolkit/<Save>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__32__ctor_m67BD95576F3B8D778601ABF047E4FAD9813640D8 (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.Void NativeToolkit::pickImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754 (const RuntimeMethod* method);
// UnityEngine.Texture2D NativeToolkit::LoadImageFromFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Action`2<UnityEngine.Texture2D,System.String>::Invoke(!0,!1)
inline void Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8 (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___arg10, String_t* ___arg21, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, String_t*, const RuntimeMethod*))Action_2_Invoke_m1738FFAE74BE5E599FD42520FA2BEF69D1AC4709_gshared)(__this, ___arg10, ___arg21, method);
}
// System.Void NativeToolkit::openCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7 (const RuntimeMethod* method);
// System.Void NativeToolkit::showAlert(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B (String_t* ___title0, String_t* ___message1, String_t* ___confirmBtnText2, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
inline void Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, bool ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, bool, const RuntimeMethod*))Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_gshared)(__this, ___obj0, method);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* File_ReadAllBytes_mF29468CED0B7B3B7C0971ACEBB16A38683718BEC (String_t* ___path0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m94295492E96C38984406A23CC2A3931758ECE86B (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data1, const RuntimeMethod* method);
// System.Void NativeToolkit/<Wait>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__40__ctor_mF3184DE97B1F4017C0FFD3D7328B554244B45699 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.Texture2D>::Invoke(!0)
inline void Action_1_Invoke_m4FDEC794781FBC6B576389503CE9A9B7ED1A06AB (Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2 (const RuntimeMethod* method);
// System.String System.DateTime::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DateTime_ToString_m203C5710CD7AB2F5F1B2D9DA1DFD45BB3774179A (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, String_t* ___format0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllBytes_m07F13C1CA0BD0960392C78AB99E0F19564F9B594 (String_t* ___path0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes1, const RuntimeMethod* method);
// System.Int32 NativeToolkit::saveToGallery(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC (String_t* ___path0, const RuntimeMethod* method);
// System.Collections.IEnumerator NativeToolkit::Wait(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, float ___delay0, const RuntimeMethod* method);
// System.Void UnityEngine.iOS.Device::SetNoBackupFlag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Device_SetNoBackupFlag_m602A3BAEBA6C0EA4925A19CF0119D70BE4209D69 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::Invoke(!0)
inline void Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, String_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, String_t*, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single AngleCalculator::AngleTwoPoint(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___one0;
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = ___two1;
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___two1;
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = ___one0;
		NullCheck(L_9);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_9, /*hidden argument*/NULL);
		float L_11 = L_10.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = ___one0;
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = ___two1;
		NullCheck(L_15);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_15, /*hidden argument*/NULL);
		float L_17 = L_16.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = ___one0;
		NullCheck(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = ___two1;
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_21, /*hidden argument*/NULL);
		float L_23 = L_22.get_y_3();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_17)), (float)((float)il2cpp_codegen_multiply((float)L_20, (float)L_23))));
		float L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_25 = atan2f(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_5)), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_11)))), L_24);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_25, (float)(57.2957764f)));
		float L_26 = V_1;
		if ((((float)L_26) < ((float)(0.0f))))
		{
			goto IL_0077;
		}
	}
	{
		float L_27 = V_1;
		G_B3_0 = L_27;
		goto IL_007e;
	}

IL_0077:
	{
		float L_28 = V_1;
		G_B3_0 = ((float)il2cpp_codegen_add((float)L_28, (float)(360.0f)));
	}

IL_007e:
	{
		V_1 = G_B3_0;
		float L_29 = V_1;
		return L_29;
	}
}
// System.Single AngleCalculator::AngleThreePoint(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___one1, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___two2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___main0;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = ___one1;
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_2, L_5, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = ___main0;
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___two2;
		NullCheck(L_10);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_11, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_9, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = V_0;
		float L_15 = Vector3_Angle_m8911FFA1DD1C8C46D923B52645B352FA1521CD5F(L_6, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void AngleCalculator::DrawLine(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___main0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___obj1, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___line2, float ___sizeLine3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___obj1;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_1, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = ___main0;
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = ___line2;
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_7, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_forward_m02858E8B3313B27174B19E9113F24EF25FBCEC7F(L_8, L_9, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_10, (2.0f), /*hidden argument*/NULL);
		V_1 = L_11;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = ___line2;
		NullCheck(L_12);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_12, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = ___main0;
		NullCheck(L_14);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_15, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_13, L_18, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = ___line2;
		NullCheck(L_19);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_19, /*hidden argument*/NULL);
		float L_21 = ___sizeLine3;
		float L_22 = ___sizeLine3;
		float L_23 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_24), L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_20, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AngleCalculator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AngleCalculator__ctor_m845B37C4E93EA0E798B32995421F0E8E8D816E54 (AngleCalculator_tDACEF6B6E9AA9576D1B11E23FC4306DEA31388C2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraIOS::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770 (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS_Start_mB85A08102CF9D15DB2A7894FA2868EFE55EA7770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Activate_5();
		bool L_1 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_0, _stringLiteral69B1910FDC4121E685DFD6A556FC93F1F8206505, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01(__this, /*hidden argument*/NULL);
		return;
	}

IL_0019:
	{
		String_t* L_2 = __this->get_Activate_5();
		bool L_3 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_2, _stringLiteralEFFC8EA4C80645993C7BBF3A87CEEF006DD7F41D, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void CameraIOS::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS_OnEnable_m5C36B59A7589ADE6ADDB96C18862EB9D381E5F9D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3(L_0, __this, (intptr_t)((intptr_t)CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3_RuntimeMethod_var);
		NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352(L_0, /*hidden argument*/NULL);
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)il2cpp_codegen_object_new(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var);
		Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913(L_1, __this, (intptr_t)((intptr_t)CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913_RuntimeMethod_var);
		NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C(L_1, /*hidden argument*/NULL);
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)il2cpp_codegen_object_new(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var);
		Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913(L_2, __this, (intptr_t)((intptr_t)CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913_RuntimeMethod_var);
		NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS_OnDisable_m0B57DE1474B6F17AB9C2973D337B95AB8AF3FEFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3(L_0, __this, (intptr_t)((intptr_t)CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m4F1B6EE6AB328B8B63B3B2CD8FB89A119C3143F3_RuntimeMethod_var);
		NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F(L_0, /*hidden argument*/NULL);
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)il2cpp_codegen_object_new(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var);
		Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913(L_1, __this, (intptr_t)((intptr_t)CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913_RuntimeMethod_var);
		NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE(L_1, /*hidden argument*/NULL);
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)il2cpp_codegen_object_new(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var);
		Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913(L_2, __this, (intptr_t)((intptr_t)CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m3D47B535B823F1C30134BFCE1849438D3FE73913_RuntimeMethod_var);
		NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::OnSaveScreenshotPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS_OnSaveScreenshotPress_m489C1444EBF81AAB8D375BF29F2966625FE3E0EB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE ));
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_0 = V_0;
		NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB(_stringLiteral406ADB4AA806CB511098281A7FF1113D10B10ACA, _stringLiteralF04CAE694D5225DFD7F7A62D33B7CD4B90717134, _stringLiteral14993722CFD06F0E85FCC82A9DC489400BEEABB9, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::OnCameraPress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnCameraPress_m0314695BCE8A2855E1C36505BAB5CAF25A43BA07 (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	{
		NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E(/*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::OnPickImagePress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_OnPickImagePress_m906960F31A96C3984BEE02DC74EFAB7C5AAECF01 (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	{
		NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE(/*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::ScreenshotSaved(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS_ScreenshotSaved_mF225FC6F291F8F97EC0ACF65746964681D66B3BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2(_stringLiteralF942D1239DAD74CB19F1D94C6877FF651D007775, _stringLiteral96D7BA069613DE9BA657F01A2C8471BC0DE8C407, (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)NULL, _stringLiteralB0A98216A32426B9E66A4AC1EB6DF2E96E1B495C, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::CameraShotComplete(UnityEngine.Texture2D,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_CameraShotComplete_m72FD4C1373AB4B5AA5EC9A810FACD4461D73D7BF (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___img0, String_t* ___path1, const RuntimeMethod* method)
{
	{
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_0 = __this->get_Photo_4();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_1 = ___img0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = ___img0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = ___img0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_6), (0.0f), (0.0f), (((float)((float)L_3))), (((float)((float)L_5))), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_7), (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_8 = Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843(L_1, L_6, L_7, (100.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_set_sprite_m0D1DBEFD8E361B3967964C71F93EF47E2AC53A13(L_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::ImagePicked(UnityEngine.Texture2D,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS_ImagePicked_mCE4BDF6C1DCB7F1DECE947A3AC2A8B64DC360ECE (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___img0, String_t* ___path1, const RuntimeMethod* method)
{
	{
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_0 = __this->get_Photo_4();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_1 = ___img0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = ___img0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = ___img0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_6), (0.0f), (0.0f), (((float)((float)L_3))), (((float)((float)L_5))), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_7), (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_8 = Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843(L_1, L_6, L_7, (100.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_set_sprite_m0D1DBEFD8E361B3967964C71F93EF47E2AC53A13(L_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraIOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE (CameraIOS_tF779A1D7BB4E452A038FE044C7E9F6B711394596 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraIOS__ctor_m3039ED4EA1CEDEE08D189A67D7317B00FDB3B5EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_Activate_5(_stringLiteralB22D24206653FBF96FAD4AFEB724C4A2C2FDC933);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DragMouse::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663 (DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragMouse_Start_mA274556FDD6A3836FAD0676CE0EC7B60EA412663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Object_FindObjectOfType_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m807C070DCBD260C48DCA9A6267F38EF36112604A(/*hidden argument*/Object_FindObjectOfType_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m807C070DCBD260C48DCA9A6267F38EF36112604A_RuntimeMethod_var);
		__this->set_MainCamera_4(L_0);
		return;
	}
}
// System.Void DragMouse::OnMouseDrag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7 (DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragMouse_OnMouseDrag_m4044569A8C409998BBBD299E79CEAC5D7F4E84A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_id_9();
		((DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields*)il2cpp_codegen_static_fields_for(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_il2cpp_TypeInfo_var))->set_idPoint_8(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Input_get_mousePosition_m1F6706785983B41FE8D5CBB81B5F15F68EBD9A53(/*hidden argument*/NULL);
		__this->set_poseInput_5(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_2 = __this->get_address_of_poseInput_5();
		float L_3 = __this->get_z_7();
		L_2->set_z_4(L_3);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_4 = __this->get_MainCamera_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = __this->get_poseInput_5();
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_4, L_5, /*hidden argument*/NULL);
		__this->set_poseTarget_6(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = __this->get_poseTarget_6();
		NullCheck(L_7);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragMouse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragMouse__ctor_m50A6B2A45958A4A88B55841255E8B1CDCBC7C462 (DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Update_mB18435F2FDDC6175F5A83AB40198635F26D6FBFB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields*)il2cpp_codegen_static_fields_for(ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_il2cpp_TypeInfo_var))->get_ModeAngle_4();
		GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::EnabledMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231 (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_EnabledMode_mD4E7912D275DA91623D4E5677F3DE70F3AB43231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ___mode0;
		if ((((int32_t)L_0) > ((int32_t)1)))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_1 = ___mode0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_2 = ___mode0;
		G_B4_0 = L_2;
		goto IL_000f;
	}

IL_000b:
	{
		int32_t L_3 = 0;
		___mode0 = L_3;
		G_B4_0 = L_3;
	}

IL_000f:
	{
		___mode0 = G_B4_0;
		int32_t L_4 = ___mode0;
		if (L_4)
		{
			goto IL_0099;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_main_5();
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)1, /*hidden argument*/NULL);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_7 = __this->get_angleText_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = __this->get_main_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = __this->get_one_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = __this->get_two_7();
		float L_11 = AngleCalculator_AngleThreePoint_m06562A7B90CB3BF7BC6C3A89E4FDDC255BE5B8CF(L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF, /*hidden argument*/NULL);
		String_t* L_13 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_12, _stringLiteral55D0C1BB201EC0F84A70A5B21BEE3C1BEDD44749, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(69 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = __this->get_main_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = __this->get_one_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = __this->get_lineOne_8();
		AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7(L_14, L_15, L_16, (4.0f), /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = __this->get_main_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = __this->get_two_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = __this->get_lineTwo_9();
		AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7(L_17, L_18, L_19, (4.0f), /*hidden argument*/NULL);
	}

IL_0099:
	{
		int32_t L_20 = ___mode0;
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_01d8;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = __this->get_main_5();
		NullCheck(L_21);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_22, (bool)0, /*hidden argument*/NULL);
		int32_t L_23 = ((DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields*)il2cpp_codegen_static_fields_for(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_il2cpp_TypeInfo_var))->get_idPoint_8();
		int32_t L_24 = L_23;
		RuntimeObject * L_25 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_25, /*hidden argument*/NULL);
		int32_t L_26 = ((DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields*)il2cpp_codegen_static_fields_for(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_il2cpp_TypeInfo_var))->get_idPoint_8();
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_0130;
		}
	}
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_27 = __this->get_angleText_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = __this->get_one_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = __this->get_three_10();
		float L_30 = AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9(L_28, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		String_t* L_31 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_32 = __this->get_one_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = __this->get_three_10();
		float L_34 = AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9(L_32, L_33, /*hidden argument*/NULL);
		V_0 = ((float)((float)((float)il2cpp_codegen_subtract((float)(360.0f), (float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_34))))/(float)(2.0f)));
		String_t* L_35 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF, /*hidden argument*/NULL);
		String_t* L_36 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_31, _stringLiteral00DB7105B91D9A97273A38086D23BE5522570D93, L_35, _stringLiteral4018E8848406C17CAE50BFB035BBE7F8410CF52B, /*hidden argument*/NULL);
		NullCheck(L_27);
		VirtActionInvoker1< String_t* >::Invoke(69 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_27, L_36);
	}

IL_0130:
	{
		int32_t L_37 = ((DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_StaticFields*)il2cpp_codegen_static_fields_for(DragMouse_t7CF71B813DA03E8B97EDD864C1932288C041821A_il2cpp_TypeInfo_var))->get_idPoint_8();
		if ((!(((uint32_t)L_37) == ((uint32_t)2))))
		{
			goto IL_01a0;
		}
	}
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_38 = __this->get_angleText_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = __this->get_two_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_40 = __this->get_four_11();
		float L_41 = AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9(L_39, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		String_t* L_42 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = __this->get_two_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = __this->get_four_11();
		float L_45 = AngleCalculator_AngleTwoPoint_mF2D44041B92ACCF4711F702D7D2A831A01D18EE9(L_43, L_44, /*hidden argument*/NULL);
		V_0 = ((float)((float)((float)il2cpp_codegen_subtract((float)(360.0f), (float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_45))))/(float)(2.0f)));
		String_t* L_46 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_0), _stringLiteral38F6D7875E3195BDAEE448D2CB6917F3AE4994AF, /*hidden argument*/NULL);
		String_t* L_47 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_42, _stringLiteral00DB7105B91D9A97273A38086D23BE5522570D93, L_46, _stringLiteral4018E8848406C17CAE50BFB035BBE7F8410CF52B, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(69 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, L_47);
	}

IL_01a0:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = __this->get_one_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_49 = __this->get_two_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_50 = __this->get_lineOne_8();
		AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7(L_48, L_49, L_50, (4.0f), /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_51 = __this->get_three_10();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_52 = __this->get_four_11();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_53 = __this->get_lineTwo_9();
		AngleCalculator_DrawLine_m5B8E1C66C986E7CDAE2138AD9395373ACDB7ECA7(L_51, L_52, L_53, (4.0f), /*hidden argument*/NULL);
	}

IL_01d8:
	{
		return;
	}
}
// System.Void GameManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager__ctor_mFBEDEFD70BE58F3D3BE07FA8F9D97DE156D5C358 (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManagerUI::OpenScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerUI_OpenScene_m5BA6194AFD241A7EE0D921E7CEBE0B55886B6604 (ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE * __this, int32_t ___sceneNumber0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___sceneNumber0;
		SceneManager_LoadScene_m84E39F1210BA4482CF2D8A2F068198760DE4B4C5(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ManagerUI::AngleMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E (ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagerUI_AngleMode_m91E2E22909B97C7AD38B7691B4987895F1FA9D3E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___mode0;
		((ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_StaticFields*)il2cpp_codegen_static_fields_for(ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE_il2cpp_TypeInfo_var))->set_ModeAngle_4(L_0);
		return;
	}
}
// System.Void ManagerUI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerUI__ctor_m80A5DD6AE73EA6774FA54E8107C66D9204E5DF50 (ManagerUI_t8A4376A041DF1478D0AADCB2DCFAA27F1EFDBABE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeToolkit::add_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4 (Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnScreenshotTaken_m382A739D9EB1CF534CE6AE0FA37DE287FB9209F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_0 = NULL;
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_1 = NULL;
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_2 = NULL;
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotTaken_4();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_2 = V_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77_il2cpp_TypeInfo_var));
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_5 = V_2;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_6 = V_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_7 = InterlockedCompareExchangeImpl<Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *>((Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnScreenshotTaken_4()), L_5, L_6);
		V_0 = L_7;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_8 = V_0;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)L_8) == ((RuntimeObject*)(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1 (Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnScreenshotTaken_mBE633DDCAD20D528ED9A6C5E730112CC270D9EF1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_0 = NULL;
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_1 = NULL;
	Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * V_2 = NULL;
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotTaken_4();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_2 = V_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77_il2cpp_TypeInfo_var));
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_5 = V_2;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_6 = V_1;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_7 = InterlockedCompareExchangeImpl<Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *>((Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnScreenshotTaken_4()), L_5, L_6);
		V_0 = L_7;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_8 = V_0;
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)L_8) == ((RuntimeObject*)(Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::add_OnScreenshotSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnScreenshotSaved_mFB79D8FD9F084CDBFF17C92E5786C379353F4352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotSaved_5();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnScreenshotSaved_5()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnScreenshotSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnScreenshotSaved_m3801D826BC78DA69C3FCCE2DF3ABCA58ED95C39F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotSaved_5();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnScreenshotSaved_5()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::add_OnImageSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnImageSaved_m8074C75D07572366B06C2B2FA11E9C3D707018E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImageSaved_6();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnImageSaved_6()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnImageSaved(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnImageSaved_m65170D1D8AF3B353AB135876DC64B7AAB70A6F35_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImageSaved_6();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnImageSaved_6()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::add_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnImagePicked_m5960C75DB79518AF9B9B2394EF6E060CF4A6D4EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_0 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_1 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_2 = NULL;
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImagePicked_7();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)CastclassSealed((RuntimeObject*)L_4, Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var));
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_5 = V_2;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_6 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_7 = InterlockedCompareExchangeImpl<Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *>((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnImagePicked_7()), L_5, L_6);
		V_0 = L_7;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_8 = V_0;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_8) == ((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnImagePicked(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476 (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnImagePicked_mD588BD9D319231648BEF286CB836604A57395476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_0 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_1 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_2 = NULL;
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImagePicked_7();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)CastclassSealed((RuntimeObject*)L_4, Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var));
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_5 = V_2;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_6 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_7 = InterlockedCompareExchangeImpl<Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *>((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnImagePicked_7()), L_5, L_6);
		V_0 = L_7;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_8 = V_0;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_8) == ((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::add_OnDialogComplete(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnDialogComplete_m9A329B9D6A4F5EC62898E86F4EE8C25E234A53BC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_0 = NULL;
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_1 = NULL;
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_2 = NULL;
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnDialogComplete_8();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = V_0;
		V_1 = L_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_2 = V_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)CastclassSealed((RuntimeObject*)L_4, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD_il2cpp_TypeInfo_var));
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_5 = V_2;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_6 = V_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_7 = InterlockedCompareExchangeImpl<Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *>((Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnDialogComplete_8()), L_5, L_6);
		V_0 = L_7;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_8 = V_0;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)L_8) == ((RuntimeObject*)(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnDialogComplete(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnDialogComplete_mFED2D44B83DC19F8FA08B6ADE92CFB8595C208FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_0 = NULL;
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_1 = NULL;
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_2 = NULL;
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnDialogComplete_8();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = V_0;
		V_1 = L_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_2 = V_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)CastclassSealed((RuntimeObject*)L_4, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD_il2cpp_TypeInfo_var));
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_5 = V_2;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_6 = V_1;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_7 = InterlockedCompareExchangeImpl<Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *>((Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnDialogComplete_8()), L_5, L_6);
		V_0 = L_7;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_8 = V_0;
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)L_8) == ((RuntimeObject*)(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::add_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_add_OnCameraShotComplete_m881A4E8CAAC4D42A6593E25A9117F730A1182D7C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_0 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_1 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_2 = NULL;
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnCameraShotComplete_9();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)CastclassSealed((RuntimeObject*)L_4, Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var));
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_5 = V_2;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_6 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_7 = InterlockedCompareExchangeImpl<Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *>((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnCameraShotComplete_9()), L_5, L_6);
		V_0 = L_7;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_8 = V_0;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_8) == ((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void NativeToolkit::remove_OnCameraShotComplete(System.Action`2<UnityEngine.Texture2D,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE (Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_remove_OnCameraShotComplete_mB4785A1751D8F2EC0CF90B22DBDB55F0432A3EAE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_0 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_1 = NULL;
	Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * V_2 = NULL;
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnCameraShotComplete_9();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)CastclassSealed((RuntimeObject*)L_4, Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40_il2cpp_TypeInfo_var));
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_5 = V_2;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_6 = V_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_7 = InterlockedCompareExchangeImpl<Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *>((Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 **)(((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_address_of_OnCameraShotComplete_9()), L_5, L_6);
		V_0 = L_7;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_8 = V_0;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_8) == ((RuntimeObject*)(Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL saveToGallery(char*);
// System.Int32 NativeToolkit::saveToGallery(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC (String_t* ___path0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(saveToGallery)(____path0_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL pickImage();
// System.Void NativeToolkit::pickImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(pickImage)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL openCamera();
// System.Void NativeToolkit::openCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(openCamera)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL showAlert(char*, char*, char*);
// System.Void NativeToolkit::showAlert(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B (String_t* ___title0, String_t* ___message1, String_t* ___confirmBtnText2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___title0' to native representation
	char* ____title0_marshaled = NULL;
	____title0_marshaled = il2cpp_codegen_marshal_string(___title0);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Marshaling of parameter '___confirmBtnText2' to native representation
	char* ____confirmBtnText2_marshaled = NULL;
	____confirmBtnText2_marshaled = il2cpp_codegen_marshal_string(___confirmBtnText2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(showAlert)(____title0_marshaled, ____message1_marshaled, ____confirmBtnText2_marshaled);

	// Marshaling cleanup of parameter '___title0' native representation
	il2cpp_codegen_marshal_free(____title0_marshaled);
	____title0_marshaled = NULL;

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

	// Marshaling cleanup of parameter '___confirmBtnText2' native representation
	il2cpp_codegen_marshal_free(____confirmBtnText2_marshaled);
	____confirmBtnText2_marshaled = NULL;

}
// NativeToolkit NativeToolkit::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_instance_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_2, /*hidden argument*/NULL);
		((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->set_go_11(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_go_11();
		NullCheck(L_3);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_3, _stringLiteral4FB88D19D18BCD0C313F5FC4A552B4A8CC64282F, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_go_11();
		NullCheck(L_4);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_5 = GameObject_AddComponent_TisNativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_mA461E7FFC6C262734D7D72C67BD8FCC3F351020A(L_4, /*hidden argument*/GameObject_AddComponent_TisNativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_mA461E7FFC6C262734D7D72C67BD8FCC3F351020A_RuntimeMethod_var);
		((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->set_instance_10(L_5);
	}

IL_0035:
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_6 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_instance_10();
		return L_6;
	}
}
// System.Void NativeToolkit::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76 (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_instance_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_2 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_instance_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void NativeToolkit::SaveScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB (String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_SaveScreenshot_mA2D895F2791069BCB74C7D83DEE8073C426B41CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		String_t* L_0 = ___fileName0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral9B9AEF62C4AAC4568E56105AE46134B32107BBDB, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_2 = ___screenArea3;
		il2cpp_codegen_initobj((&V_0), sizeof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE ));
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_3 = V_0;
		bool L_4 = Rect_op_Equality_mFBE3505CEDD6B73F66276E782C1B02E0E5633563(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_5 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_6 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&___screenArea3), (0.0f), (0.0f), (((float)((float)L_5))), (((float)((float)L_6))), /*hidden argument*/NULL);
	}

IL_003e:
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_7 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_8 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		String_t* L_9 = ___fileName0;
		String_t* L_10 = ___albumName1;
		String_t* L_11 = ___fileType2;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_12 = ___screenArea3;
		NullCheck(L_8);
		RuntimeObject* L_13 = NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA(L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_7, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator NativeToolkit::GrabScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_GrabScreenshot_mD206B16050703283D757478E80179D63C70BF3AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * L_0 = (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 *)il2cpp_codegen_object_new(U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5_il2cpp_TypeInfo_var);
		U3CGrabScreenshotU3Ed__30__ctor_mE9C27BC1166709434CAF40D4004ABAB4E3E84597(L_0, 0, /*hidden argument*/NULL);
		U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * L_1 = L_0;
		String_t* L_2 = ___fileName0;
		NullCheck(L_1);
		L_1->set_fileName_4(L_2);
		U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * L_3 = L_1;
		String_t* L_4 = ___fileType2;
		NullCheck(L_3);
		L_3->set_fileType_3(L_4);
		U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * L_5 = L_3;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_6 = ___screenArea3;
		NullCheck(L_5);
		L_5->set_screenArea_2(L_6);
		return L_5;
	}
}
// System.Void NativeToolkit::SaveImage(UnityEngine.Texture2D,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, String_t* ___fileName1, String_t* ___fileType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_SaveImage_m392CBA6D135294C1BC858C31A0B90789D9E758DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ___fileName1;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralE768FFA35BEFAEE87A2CB4ED20194AAFD3A43C5C, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_2 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NullCheck(L_2);
		NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76(L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___fileType2;
		bool L_4 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_3, _stringLiteral9040A7D6CDF7A0D6CAB1823831C6CEB7D01AF97F, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = ___texture0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ImageConversion_EncodeToPNG_m8D67A36A7D81F436CDA108CC5293E15A9CFD5617(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		V_1 = _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0;
		goto IL_0043;
	}

IL_0036:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_7 = ___texture0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = ImageConversion_EncodeToJPG_mDE6C67AACCC7BAD006D0A852093F881D11F9C8DF(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		V_1 = _stringLiteralDCDFD41CB6E96E78736DEB5F5ADD6F6311DA8E80;
	}

IL_0043:
	{
		String_t* L_9 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_10 = ___fileName1;
		String_t* L_11 = V_1;
		String_t* L_12 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_9, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_13 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_14 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_0;
		String_t* L_16 = ___fileName1;
		String_t* L_17 = V_2;
		NullCheck(L_14);
		RuntimeObject* L_18 = NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F(L_14, L_15, L_16, L_17, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_13, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator NativeToolkit::Save(System.Byte[],System.String,System.String,NativeToolkit_ImageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes0, String_t* ___fileName1, String_t* ___path2, int32_t ___imageType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * L_0 = (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD *)il2cpp_codegen_object_new(U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD_il2cpp_TypeInfo_var);
		U3CSaveU3Ed__32__ctor_m67BD95576F3B8D778601ABF047E4FAD9813640D8(L_0, 0, /*hidden argument*/NULL);
		U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * L_1 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___bytes0;
		NullCheck(L_1);
		L_1->set_bytes_3(L_2);
		U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * L_3 = L_1;
		String_t* L_4 = ___path2;
		NullCheck(L_3);
		L_3->set_path_2(L_4);
		U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * L_5 = L_3;
		int32_t L_6 = ___imageType3;
		NullCheck(L_5);
		L_5->set_imageType_4(L_6);
		return L_5;
	}
}
// System.Void NativeToolkit::PickImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_PickImage_mCA4C91F93303E18E5525F02CCA39AEC8ADEFE2AE (const RuntimeMethod* method)
{
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_0 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NullCheck(L_0);
		NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76(L_0, /*hidden argument*/NULL);
		int32_t L_1 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0017;
		}
	}
	{
		NativeToolkit_pickImage_mDC21C9AB1E1B1B350691C850CD4A365B1B278754(/*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void NativeToolkit::OnPickImage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30 (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_OnPickImage_mE6AB5C4B058F31EA160A5A118490B9BDE56B8E30_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_1 = NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImagePicked_7();
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImagePicked_7();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = V_0;
		String_t* L_5 = ___path0;
		NullCheck(L_3);
		Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void NativeToolkit::TakeCameraShot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_TakeCameraShot_m64BEFAF0B81BBC17D8360B4666F91888498F681E (const RuntimeMethod* method)
{
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_0 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NullCheck(L_0);
		NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76(L_0, /*hidden argument*/NULL);
		int32_t L_1 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0017;
		}
	}
	{
		NativeToolkit_openCamera_mBDACD79CF24073E5942A35F13FF6B9185A1C61A7(/*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void NativeToolkit::OnCameraFinished(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_OnCameraFinished_m48BEE6DD182FBE2899C28AE35D7B49DDE130CBDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_1 = NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_2 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnCameraShotComplete_9();
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Action_2_t492EE5D223A0FD6A98BE8EAB6B823654CDAD0C40 * L_3 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnCameraShotComplete_9();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = V_0;
		String_t* L_5 = ___path0;
		NullCheck(L_3);
		Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_mD27F314040FE36AE3B6CBD4D4E773C20F5BA67A8_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void NativeToolkit::ShowAlert(System.String,System.String,System.Action`1<System.Boolean>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2 (String_t* ___title0, String_t* ___message1, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback2, String_t* ___btnText3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_ShowAlert_mFE9CB467A15F22EEF4295F7DF1289972673F90C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_0 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NullCheck(L_0);
		NativeToolkit_Awake_m754F7754813ECA7D4C23F8A30A2810A50B111A76(L_0, /*hidden argument*/NULL);
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = ___callback2;
		((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->set_OnDialogComplete_8(L_1);
		int32_t L_2 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)8))))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___title0;
		String_t* L_4 = ___message1;
		String_t* L_5 = ___btnText3;
		NativeToolkit_showAlert_m511C48E6784BD122AD5B130D7ECD55F5F7D7798B(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void NativeToolkit::OnDialogPress(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, String_t* ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_OnDialogPress_m611B3ECADDBB35FD09F7EE514822E7E5B07D6B5E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_0 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnDialogComplete_8();
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_1 = ___result0;
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteral5397E0583F14F6C88DE06B1EF28F460A1FB5B0AE, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_3 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnDialogComplete_8();
		NullCheck(L_3);
		Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F(L_3, (bool)1, /*hidden argument*/Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var);
		return;
	}

IL_0020:
	{
		String_t* L_4 = ___result0;
		bool L_5 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_4, _stringLiteral816C52FD2BDD94A63CD0944823A6C0AA9384C103, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_6 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnDialogComplete_8();
		NullCheck(L_6);
		Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F(L_6, (bool)0, /*hidden argument*/Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var);
	}

IL_0038:
	{
		return;
	}
}
// UnityEngine.Texture2D NativeToolkit::LoadImageFromFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0 (String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_LoadImageFromFile_m76DD2CA941C921D7F1E42BB001D535A7F4F044E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		bool L_1 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_0, _stringLiteralA1BF92EFF40DD4ADEA2328F9E0F7C938AE031DEC, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)NULL;
	}

IL_000f:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_2, ((int32_t)128), ((int32_t)128), 3, (bool)0, /*hidden argument*/NULL);
		String_t* L_3 = ___path0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = File_ReadAllBytes_mF29468CED0B7B3B7C0971ACEBB16A38683718BEC(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = L_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_0;
		ImageConversion_LoadImage_m94295492E96C38984406A23CC2A3931758ECE86B(L_5, L_6, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.IEnumerator NativeToolkit::Wait(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, float ___delay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * L_0 = (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 *)il2cpp_codegen_object_new(U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8_il2cpp_TypeInfo_var);
		U3CWaitU3Ed__40__ctor_mF3184DE97B1F4017C0FFD3D7328B554244B45699(L_0, 0, /*hidden argument*/NULL);
		U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * L_1 = L_0;
		float L_2 = ___delay0;
		NullCheck(L_1);
		L_1->set_delay_2(L_2);
		return L_1;
	}
}
// System.Void NativeToolkit::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeToolkit__ctor_m385E36B7410BADB983BB42E07067986E46F3CEFC (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeToolkit_<GrabScreenshot>d__30::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGrabScreenshotU3Ed__30__ctor_mE9C27BC1166709434CAF40D4004ABAB4E3E84597 (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void NativeToolkit_<GrabScreenshot>d__30::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGrabScreenshotU3Ed__30_System_IDisposable_Dispose_m8AEC7B08175BA231FB919A5A885B4977D603FBA6 (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean NativeToolkit_<GrabScreenshot>d__30::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGrabScreenshotU3Ed__30_MoveNext_m988CDF05BFAC0D86EA8E632E17B54DC33E28FC7C (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGrabScreenshotU3Ed__30_MoveNext_m988CDF05BFAC0D86EA8E632E17B54DC33E28FC7C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_3 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_002b:
	{
		__this->set_U3CU3E1__state_0((-1));
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE * L_4 = __this->get_address_of_screenArea_2();
		float L_5 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)L_4, /*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE * L_6 = __this->get_address_of_screenArea_2();
		float L_7 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)L_6, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_8 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_8, (((int32_t)((int32_t)(int32_t)L_5))), (((int32_t)((int32_t)(int32_t)L_7))), 3, (bool)0, /*hidden argument*/NULL);
		V_1 = L_8;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_9 = V_1;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_10 = __this->get_screenArea_2();
		NullCheck(L_9);
		Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61(L_9, L_10, 0, 0, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_11 = V_1;
		NullCheck(L_11);
		Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_11, /*hidden argument*/NULL);
		String_t* L_12 = __this->get_fileType_3();
		bool L_13 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_12, _stringLiteral9040A7D6CDF7A0D6CAB1823831C6CEB7D01AF97F, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0087;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_14 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = ImageConversion_EncodeToPNG_m8D67A36A7D81F436CDA108CC5293E15A9CFD5617(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		V_3 = _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0;
		goto IL_0094;
	}

IL_0087:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_16 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = ImageConversion_EncodeToJPG_mDE6C67AACCC7BAD006D0A852093F881D11F9C8DF(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		V_3 = _stringLiteralDCDFD41CB6E96E78736DEB5F5ADD6F6311DA8E80;
	}

IL_0094:
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_18 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotTaken_4();
		if (!L_18)
		{
			goto IL_00a8;
		}
	}
	{
		Action_1_t5DF2DB80B8ECC3CB04DFF47D5E1A79B8043BDC77 * L_19 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotTaken_4();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_20 = V_1;
		NullCheck(L_19);
		Action_1_Invoke_m4FDEC794781FBC6B576389503CE9A9B7ED1A06AB(L_19, L_20, /*hidden argument*/Action_1_Invoke_m4FDEC794781FBC6B576389503CE9A9B7ED1A06AB_RuntimeMethod_var);
		goto IL_00ae;
	}

IL_00a8:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_21, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var);
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_22 = DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2(/*hidden argument*/NULL);
		V_7 = L_22;
		String_t* L_23 = DateTime_ToString_m203C5710CD7AB2F5F1B2D9DA1DFD45BB3774179A((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_7), _stringLiteralA041727F44C06126C7D6079E6FEB153B861215C3, /*hidden argument*/NULL);
		V_4 = L_23;
		String_t* L_24 = __this->get_fileName_4();
		String_t* L_25 = V_4;
		String_t* L_26 = V_3;
		String_t* L_27 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_24, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, L_25, L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		String_t* L_28 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_29 = V_5;
		String_t* L_30 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_28, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, L_29, /*hidden argument*/NULL);
		V_6 = L_30;
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_31 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_32 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = V_2;
		String_t* L_34 = __this->get_fileName_4();
		String_t* L_35 = V_6;
		NullCheck(L_32);
		RuntimeObject* L_36 = NativeToolkit_Save_m140881BF9818CE3E5D437A2FB243D2F0AAD6932F(L_32, L_33, L_34, L_35, 1, /*hidden argument*/NULL);
		NullCheck(L_31);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_31, L_36, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object NativeToolkit_<GrabScreenshot>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGrabScreenshotU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m621F8D815CC1CC396D7783388B7CB95114C89593 (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object NativeToolkit_<GrabScreenshot>d__30::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGrabScreenshotU3Ed__30_System_Collections_IEnumerator_get_Current_m4353553BC4D04F4FFEAFFB984EE50E30A3D4A917 (U3CGrabScreenshotU3Ed__30_t9C4CD3FCF4244E2050EDE9211C8AF320ECF56EF5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeToolkit_<Save>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__32__ctor_m67BD95576F3B8D778601ABF047E4FAD9813640D8 (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void NativeToolkit_<Save>d__32::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__32_System_IDisposable_Dispose_m37C5DFF6EB8DA62D3801BF4821EC0605AB854F68 (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean NativeToolkit_<Save>d__32::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSaveU3Ed__32_MoveNext_mE2C978BE5590BFF0D016F75D9DD24AAB8CB566ED (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveU3Ed__32_MoveNext_mE2C978BE5590BFF0D016F75D9DD24AAB8CB566ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00a2;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		__this->set_U3CU3E1__state_0((-1));
		__this->set_U3CcountU3E5__2_5(0);
		__this->set_U3CsavedU3E5__3_6(0);
		int32_t L_3 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00bc;
		}
	}
	{
		String_t* L_4 = __this->get_path_2();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get_bytes_3();
		File_WriteAllBytes_m07F13C1CA0BD0960392C78AB99E0F19564F9B594(L_4, L_5, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_0046:
	{
		int32_t L_6 = __this->get_U3CcountU3E5__2_5();
		V_1 = L_6;
		int32_t L_7 = V_1;
		__this->set_U3CcountU3E5__2_5(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		int32_t L_8 = __this->get_U3CcountU3E5__2_5();
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)30))))
		{
			goto IL_0069;
		}
	}
	{
		__this->set_U3CsavedU3E5__3_6(3);
		goto IL_007a;
	}

IL_0069:
	{
		String_t* L_9 = __this->get_path_2();
		int32_t L_10 = NativeToolkit_saveToGallery_m03FBD6F037E27E5C07E0C069EF04A1943C667ECC(L_9, /*hidden argument*/NULL);
		__this->set_U3CsavedU3E5__3_6(L_10);
	}

IL_007a:
	{
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_11 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * L_12 = NativeToolkit_get_Instance_m41CDB2CA3C0AA5857CB2259D9D60AA6854D324D6(/*hidden argument*/NULL);
		NullCheck(L_12);
		RuntimeObject* L_13 = NativeToolkit_Wait_m4E861D281BB590D1DF6C134E20A31595A6B77EAE(L_12, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * L_14 = MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_11, L_13, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_14);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00a2:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00a9:
	{
		int32_t L_15 = __this->get_U3CsavedU3E5__3_6();
		if (!L_15)
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_16 = __this->get_path_2();
		Device_SetNoBackupFlag_m602A3BAEBA6C0EA4925A19CF0119D70BE4209D69(L_16, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		int32_t L_17 = __this->get_U3CsavedU3E5__3_6();
		V_2 = L_17;
		int32_t L_18 = V_2;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			goto IL_00cd;
		}
	}
	{
		int32_t L_19 = V_2;
		if ((((int32_t)L_19) == ((int32_t)3)))
		{
			goto IL_00da;
		}
	}
	{
		goto IL_00e5;
	}

IL_00cd:
	{
		__this->set_path_2(_stringLiteralD521A4D20A49C36E58B0788F07F2834210EC2C07);
		goto IL_00e5;
	}

IL_00da:
	{
		__this->set_path_2(_stringLiteral6EC6121CD150A73105F36A36219F4E978FE2E3DC);
	}

IL_00e5:
	{
		int32_t L_20 = __this->get_imageType_4();
		V_3 = L_20;
		int32_t L_21 = V_3;
		if (!L_21)
		{
			goto IL_00f5;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) == ((int32_t)1)))
		{
			goto IL_010e;
		}
	}
	{
		goto IL_0125;
	}

IL_00f5:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_23 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImageSaved_6();
		if (!L_23)
		{
			goto IL_0125;
		}
	}
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_24 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnImageSaved_6();
		String_t* L_25 = __this->get_path_2();
		NullCheck(L_24);
		Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F(L_24, L_25, /*hidden argument*/Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var);
		goto IL_0125;
	}

IL_010e:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_26 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotSaved_5();
		if (!L_26)
		{
			goto IL_0125;
		}
	}
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_27 = ((NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields*)il2cpp_codegen_static_fields_for(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_il2cpp_TypeInfo_var))->get_OnScreenshotSaved_5();
		String_t* L_28 = __this->get_path_2();
		NullCheck(L_27);
		Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F(L_27, L_28, /*hidden argument*/Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var);
	}

IL_0125:
	{
		return (bool)0;
	}
}
// System.Object NativeToolkit_<Save>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSaveU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA888257B7430A0F0B852DF6A0E3B9AA9B589A08E (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object NativeToolkit_<Save>d__32::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSaveU3Ed__32_System_Collections_IEnumerator_get_Current_m16ACA322E86938A6BBB7355E2DDC77D4B038B150 (U3CSaveU3Ed__32_t7CDB03EAED9F32AFF48C19A2F2ECDE11E3A2EFAD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NativeToolkit_<Wait>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__40__ctor_mF3184DE97B1F4017C0FFD3D7328B554244B45699 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void NativeToolkit_<Wait>d__40::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitU3Ed__40_System_IDisposable_Dispose_mF774E32F036659DBB80F74BAACE24CE136CF9968 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean NativeToolkit_<Wait>d__40::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitU3Ed__40_MoveNext_m92EB8C93147F80177ED3C9D93AA3C69B270D6913 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		float L_4 = __this->get_delay_2();
		__this->set_U3CpauseTargetU3E5__2_3(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
		goto IL_0042;
	}

IL_002b:
	{
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_003b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0042:
	{
		float L_5 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		float L_6 = __this->get_U3CpauseTargetU3E5__2_3();
		if ((((float)L_5) < ((float)L_6)))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}
}
// System.Object NativeToolkit_<Wait>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11792DBCD4DDDC59B9FAD45527DEB7389DB22FB7 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Object NativeToolkit_<Wait>d__40::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitU3Ed__40_System_Collections_IEnumerator_get_Current_m5D28DF754C63B87510EF7B9FCFBDB422F5DD4C91 (U3CWaitU3Ed__40_tE7A4A364AD662058B049BACD3857929E6FDABDE8 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
