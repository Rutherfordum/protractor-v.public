﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraIOS : MonoBehaviour
{
    public Image Photo;
    public string Activate = "PickImagePress or CameraPress";
    private void Start()
    {
        if (Activate == "PickImagePress")
        {
            OnPickImagePress();
        }
        else if (Activate == "CameraPress")
        {
            OnCameraPress();
        }
    }

    void OnEnable()
    {
        NativeToolkit.OnScreenshotSaved += ScreenshotSaved;
        NativeToolkit.OnCameraShotComplete += CameraShotComplete;
        NativeToolkit.OnImagePicked += ImagePicked;
    }

    void OnDisable()
    {
        NativeToolkit.OnScreenshotSaved -= ScreenshotSaved;
        NativeToolkit.OnCameraShotComplete -= CameraShotComplete;
        NativeToolkit.OnImagePicked -= ImagePicked;
    }

    //=============================================================================
    // Button handlers
    //=============================================================================
    public void OnSaveScreenshotPress()
    {
        NativeToolkit.SaveScreenshot("MyScreenshot", "MyScreenshotFolder", "jpeg");
    }

    public void OnCameraPress()
    {
        NativeToolkit.TakeCameraShot();
    }

    public void OnPickImagePress()
    {
        NativeToolkit.PickImage();
    }

    //=============================================================================
    // Calback
    //=============================================================================
    void ScreenshotSaved(string path)
    {
        NativeToolkit.ShowAlert("Protractor AR", "Save the file successfully");
    }

    void CameraShotComplete(Texture2D img, string path)
    {
        Photo.sprite = Sprite.Create(img, new Rect(0f, 0f, img.width, img.height), new Vector2(0.5f, 0.5f), 100f);
    }

    void ImagePicked(Texture2D img, string path)
    {
        Photo.sprite = Sprite.Create(img, new Rect(0f, 0f, img.width, img.height), new Vector2(0.5f, 0.5f), 100f);
    }
}
