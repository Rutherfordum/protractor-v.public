﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMouse : MonoBehaviour
{
    private Camera MainCamera;
    private Vector3 poseInput,poseTarget;
    public float z;
    public static int idPoint;
    public int id;

    private void Start()
    {        
        MainCamera = GameObject.FindObjectOfType<Camera>();
    }
    /// <summary>
    /// перемещение обьектов по нажатию на них 
    /// </summary>
    private void OnMouseDrag()
    {
        idPoint = id;
        poseInput = Input.mousePosition;
        poseInput.z = z;
        poseTarget = MainCamera.ScreenToWorldPoint(poseInput);
        transform.position = poseTarget;
    }
}