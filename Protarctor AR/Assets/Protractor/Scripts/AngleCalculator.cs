﻿/*
 * класс содержит в себе методы по рассчету
 * углов по 2 точкам, так и по 3 точкам
 * а также нахождние пересечение отрезков в 
 * пространстве с нахождением точки пересечения 
 */
using UnityEngine;

public class AngleCalculator : MonoBehaviour
{
    /// <summary>
    ///  нахождение угла по 2 точкам 
    /// </summary>
    public static float AngleTwoPoint(Transform one, Transform two)
    {
        float sin = one.position.x * two.position.y - two.position.x * one.position.y;
        float cos = one.position.x * two.position.x + one.position.y * two.position.y;
        float E = Mathf.Atan2(sin, cos) * (180 / Mathf.PI);
        E = (E < 0) ? E + 360 : E;
        return E;
    }
    /// <summary>
    ///  нахождение угла по 3 точкам
    /// </summary>
    public static float AngleThreePoint(Transform main,Transform one, Transform two)
    {
        Vector3 directOne = main.transform.position - one.transform.position;
        Vector3 directTwo = main.transform.position - two.transform.position;
        return Vector3.Angle(directOne, directTwo);
    }
    /// <summary>
    /// рисовалка линий от точки до точки
    /// </summary>
    public static void DrawLine(Transform main, Transform obj, Transform line, float sizeLine = 0.005f)
    {
       
        Vector3 distance = obj.transform.localPosition - main.transform.localPosition;
        line.transform.forward = distance;
        Vector3 halfdist = distance / 2;
        line.transform.localPosition = main.transform.localPosition + halfdist;
        line.transform.localScale = new Vector3(sizeLine, sizeLine, distance.magnitude);
    }

    /// <summary>
    /// метод кооторый находит пересечения между отрезками
    /// p1, p2, p3, p4 - позиция точки 
    /// x - точка пересечения по X
    /// y - точка пересечения по Y
    /// </summary>
    public static bool Point(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, out float X, out float Y)
    {
        if (p2.x < p1.x)
        {
            Vector3 tmp = p1;
            p1 = p2;
            p2 = tmp;
        }
        if (p4.x < p3.x)
        {
            Vector3 tmp = p3;
            p3 = p4;
            p4 = tmp;
        }

        if (p2.x < p3.x)
        {
            X = 0;
            Y = 0;
            return false;
        }
        //оба отрезка вертикальные
        if ((p1.x - p2.x == 0) && (p3.x - p4.x == 0))
        {
            if (p1.x == p3.x)
            {
                if (!(Mathf.Max(p1.y, p2.y) < Mathf.Min(p3.y, p4.y)))
                {

                    X = 0;
                    Y = 0;
                    return true;
                }
            }

            X = 0;
            Y = 0;
            return false;
        }
        //первый отрезок вертикальный
        if (p1.x - p2.x == 0)
        {
            float Xa1 = p1.x;
            float A21 = (p3.y - p4.y) / (p3.x - p4.x);
            float b21 = p3.y - A21 * p3.x;
            float Ya1 = A21 * Xa1 + b21;
            if (p3.x <= Xa1 && p4.x >= Xa1 && Mathf.Min(p1.y, p2.y) <= Ya1 && Mathf.Max(p1.y, p2.y) >= Ya1)
            {
                X = Xa1;
                Y = Ya1;
                return true;
            }

            X = 0;
            Y = 0;
            return false;
        }
        //второй отрезок вертикальный
        if (p3.x - p4.x == 0)
        {
            float Xa2 = p3.x;
            float A12 = (p1.y - p2.y) / (p1.x - p2.x);
            float b12 = p1.y - A12 * p1.x;
            float Ya2 = A12 * Xa2 + b12;

            if (p1.x <= Xa2 && p2.x >= Xa2 && Mathf.Min(p3.y, p4.y) <= Ya2 && Mathf.Max(p3.y, p4.y) >= Ya2)
            {
                X = Xa2;
                Y = Ya2;
                return true;
            }

            X = 0;
            Y = 0;
            return false;
        }
        //оба отрезка не вертикальны
        float xA1 = (p1.y - p2.y) / (p1.x - p2.x);
        float xA2 = (p3.y - p4.y) / (p3.x - p4.x);
        float xb1 = p1.y - xA1 * p1.x;
        float xb2 = p3.y - xA2 * p3.x;

        float yA1 = (p1.x - p2.x) / (p1.y - p2.y);
        float yA2 = (p3.x - p4.x) / (p3.y - p4.y);
        float yb1 = p1.x - yA1 * p1.y;
        float yb2 = p3.x - yA2 * p3.y;

        if (xA1 == xA2 || yA1 == yA2)
        {

            X = 0;
            Y = 0;
            return false;
        }

        float Xa = (xb2 - xb1) / (xA1 - xA2);
        float Ya = (yb2 - yb1) / (yA1 - yA2);


        if ((Xa < Mathf.Max(p1.x, p3.x)) || (Xa > Mathf.Min(p2.x, p4.x)))
        {

            X = 0;
            Y = 0;
            return false;
        }
        else
        {
            X = Xa;
            Y = Ya;
            return true;
        }
    }
}
