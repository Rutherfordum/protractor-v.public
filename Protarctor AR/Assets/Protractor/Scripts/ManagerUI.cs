﻿/* класс предназначен для UI Event
 * события по ножатию и т.п. 
 * реализованы методы смены режима 
 * рассчета углов в пространстве 
 * а так же Смена сцены
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerUI : MonoBehaviour
{
    public static int ModeAngle = 0;

    public Transform main;
    public Transform one;
    public Transform two;
    public Transform three;
    public Transform four;
    public Transform five;

    public Color nodeOne;
    public Color nodeTwo;
    public Color nodeThree;

    /// <summary>
    /// загрузка сцены, sceneNumber-указать номер сцены
    /// </summary>
    public void OpenScene(int sceneNumber)
    {
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }

    /// <summary>
    /// режим Углов, mode(0-режим с 3 точками, 1-режим с 4 точками)
    /// </summary>
    public void AngleMode(int mode)
    {

        if (mode == 0)
        {
            if (main != null)
            {   // передача точкам новых позиций
                main.localPosition = new Vector3(-300, -300, 0);
                main.GetComponentInChildren<MeshRenderer>().material.color = nodeOne;
                one.localPosition = new Vector3(300, 300, 0);
                one.GetComponentInChildren<MeshRenderer>().material.color = nodeTwo;
                two.localPosition = new Vector3(300, -300, 0);
                two.GetComponentInChildren<MeshRenderer>().material.color = nodeTwo;
            }
        }
        
        else if (mode == 1)
        {   // передача точкам новых позиций
            main.localPosition = new Vector3(150, -300, 0);
            main.GetComponentInChildren<MeshRenderer>().material.color = nodeOne;

            one.localPosition = new Vector3(150, 300, 0);
            one.GetComponentInChildren<MeshRenderer>().material.color = nodeOne;

            two.localPosition = new Vector3(300, -150, 0);
            two.GetComponentInChildren<MeshRenderer>().material.color = nodeTwo;

            three.localPosition = new Vector3(-300, -300, 0);
            three.GetComponentInChildren<MeshRenderer>().material.color = nodeTwo;

            four.localPosition = new Vector3(300, 150, 0);
            four.GetComponentInChildren<MeshRenderer>().material.color = nodeThree;

            five.localPosition = new Vector3(-300, 150, 0);
            five.GetComponentInChildren<MeshRenderer>().material.color = nodeThree;
        }
        ModeAngle = mode;
    }
}
