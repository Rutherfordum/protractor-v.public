﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Scale Manager")]
    public GameObject ObjectSize;
    public float MaxSize, MinSize, sensitiv;

    [Header("Angle setting")]
    public Text angleText;

    [Tooltip("Center point")]
    public float X0, Y0;
    public float X1, Y1;

    [Header("Mode One")]
    public Transform AnglePink0;
    public Transform main;
    public Transform one;
    public Transform two;
    public Transform lineOne;
    public Transform lineTwo;
    public Transform lineThree;

    [Header("Mode Two")]
    public Transform three;
    public Transform four;
    public Transform five;
    public Transform _centrPoint0;
    public Transform _centrPoint1;

    private void Update()
    {
        // тупо скейл обьекта по 2 пальцам
        InputDevice.ScaleObject(ObjectSize, MinSize, MaxSize, sensitiv);

        EnabledMode(ManagerUI.ModeAngle);
    }

    /// <summary>
    /// выбор режима измерения углов
    /// 0 - это режим измерения угла по 3 точкам
    /// 1 - по 3 отрезкам
    /// </summary>
    void EnabledMode(int mode = 0)
    {
        mode = (mode > 1) || (mode < 0) ? mode = 0 : mode;

        if (mode == 0)
        {
            main.gameObject.SetActive(true);
            angleText.text = AngleCalculator.AngleThreePoint(main, one, two).ToString(format: "0.0") + '°';
            AngleCalculator.DrawLine(main, one, lineOne, 4f);
            AngleCalculator.DrawLine(main, two, lineTwo, 4f);
        }

        if (mode == 1)
        {
            main.gameObject.SetActive(true);

            // проверка что отрезки пересекаются
            bool act0 = AngleCalculator.Point(main.localPosition, one.localPosition, two.localPosition, three.localPosition, out X0, out Y0);
            bool act1 = AngleCalculator.Point(main.localPosition, one.localPosition, four.localPosition, five.localPosition, out X1, out Y1);

            float a = 0, b = 0, c = 0;

            if (act0 == true)
            {
                _centrPoint0.localPosition = new Vector3(X0, Y0, 0);
                a = AngleCalculator.AngleThreePoint(_centrPoint0, main, three);
                b = AngleCalculator.AngleThreePoint(_centrPoint0, main, two);
                Debug.Log(a);
                Debug.Log(b);
            }

            if (act1 == true)
            {
                Debug.Log("YES");
                _centrPoint1.localPosition = new Vector3(X1, Y1, 0);
                c = AngleCalculator.AngleThreePoint(_centrPoint1, one, four);
                Debug.Log(c);
            }
            // результат углов на графику
            angleText.text = c.ToString(format: "0.0") + "°\n(" + a.ToString(format: "0.0") + "° " + b.ToString(format: "0.0") + "°)";

            // методы рисующие линии от точки до точки
            AngleCalculator.DrawLine(main, one, lineOne, 4f);
            AngleCalculator.DrawLine(two, three, lineTwo, 4f);
            AngleCalculator.DrawLine(four, five, lineThree, 4f);
        }
    }

    /// <summary>
    /// метод кооторый находит пересечения между отрезками
    /// p1, p2, p3, p4 - позиция точки 
    /// x - точка пересечения по X
    /// y - точка пересечения по Y
    /// </summary>
    public bool Point(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, out float X, out float Y)
    {
        if (p2.x < p1.x)
        {
            Vector3 tmp = p1;
            p1 = p2;
            p2 = tmp;
        }
        if (p4.x < p3.x)
        {
            Vector3 tmp = p3;
            p3 = p4;
            p4 = tmp;
        }

        if (p2.x < p3.x)
        {
            X = 0;
            Y = 0;
            return false;
        }
        //оба отрезка вертикальные
        if ((p1.x - p2.x == 0) && (p3.x - p4.x == 0))
        {
            if (p1.x == p3.x)
            {
                if (!(Mathf.Max(p1.y, p2.y) < Mathf.Min(p3.y, p4.y)))
                {

                    X = 0;
                    Y = 0;
                    return true;
                }
            }

            X = 0;
            Y = 0;
            return false;
        }
        //первый отрезок вертикальный
        if (p1.x - p2.x == 0)
        {
            float Xa1 = p1.x;
            float A21 = (p3.y - p4.y) / (p3.x - p4.x);
            float b21 = p3.y - A21 * p3.x;
            float Ya1 = A21 * Xa1 + b21;
            if (p3.x <= Xa1 && p4.x >= Xa1 && Mathf.Min(p1.y, p2.y) <= Ya1 && Mathf.Max(p1.y, p2.y) >= Ya1)
            {
                X = Xa1;
                Y = Ya1;
                return true;
            }

            X = 0;
            Y = 0;
            return false;
        }
        //второй отрезок вертикальный
        if (p3.x - p4.x == 0)
        {
            float Xa2 = p3.x;
            float A12 = (p1.y - p2.y) / (p1.x - p2.x);
            float b12 = p1.y - A12 * p1.x;
            float Ya2 = A12 * Xa2 + b12;

            if (p1.x <= Xa2 && p2.x >= Xa2 && Mathf.Min(p3.y, p4.y) <= Ya2 && Mathf.Max(p3.y, p4.y) >= Ya2)
            {
                X = Xa2;
                Y = Ya2;
                return true;
            }

            X = 0;
            Y = 0;
            return false;
        }
        //оба отрезка не вертикальны
        float xA1 = (p1.y - p2.y) / (p1.x - p2.x);
        float xA2 = (p3.y - p4.y) / (p3.x - p4.x);
        float xb1 = p1.y - xA1 * p1.x;
        float xb2 = p3.y - xA2 * p3.x;

        float yA1 = (p1.x - p2.x) / (p1.y - p2.y);
        float yA2 = (p3.x - p4.x) / (p3.y - p4.y);
        float yb1 = p1.x - yA1 * p1.y;
        float yb2 = p3.x - yA2 * p3.y;

        if (xA1 == xA2 || yA1 == yA2)
        {

            X = 0;
            Y = 0;
            return false;
        }

        float Xa = (xb2 - xb1) / (xA1 - xA2);
        float Ya = (yb2 - yb1) / (yA1 - yA2);


        if ((Xa < Mathf.Max(p1.x, p3.x)) || (Xa > Mathf.Min(p2.x, p4.x)))
        {

            X = 0;
            Y = 0;
            return false;
        }
        else
        {
            X = Xa;
            Y = Ya;
            return true;
        }
    }

}
